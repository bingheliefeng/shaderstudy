## Shader学习
	里面有Demo和说明，第三方参考 https://github.com/IronWarrior
1. [漫反射] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/%E6%BC%AB%E5%8F%8D%E5%B0%84
2. [高光] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/%E9%AB%98%E5%85%89
3. [Fresnel] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Fresnel
4. [Rim 模型发光] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Rim
5. [阴影的几种方式] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Shadow
6. [视差贴图] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Parallax
7. [法线贴图] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Normal
8. [雾] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Fog
9. [Billboard] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Billboard
10. [投影] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Projector
11. [屏幕坐标空间] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/ScreenPos
12. [模型描边] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/%E6%8F%8F%E8%BE%B9
13. [PBS物理材质] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/PBS
14. [GrabPass抓屏] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/GrabPass
15. [水] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Water
16. [消融效果] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/%E6%B6%88%E8%9E%8D
17. [亮度，饱和度，对比度] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Color
18. [UV旋转和缩放] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/UV
19. [Vertex变形，跑酷，旗帜] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Vertex
20. [Toon 卡通着色] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/Toon
21. [SmoothStep方法] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/SmoothStep
22. [multi_compile和shader_feature] https://gitee.com/bingheliefeng/shaderstudy/tree/master/Assets/multi_compile
