﻿Shader "Study/Projector/Proj2"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _FallOff("Fall Off Texture",2D) = "white"{}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent"}
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 uv2 : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _FallOff;

            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = mul(unity_Projector,v.vertex);
                o.uv2 = mul(unity_ProjectorClip,v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2Dproj(_MainTex, i.uv);
                fixed4 falloff = tex2Dproj(_FallOff, i.uv2);
                return col * falloff.r;
            }
            ENDCG
        }
    }
}
