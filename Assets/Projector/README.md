## Projector 投影
    投影的Pass主要是在其他物体渲染完成后，再执行这个投影的Pass。
    在投影Shader中，只能得到投影物体的顶点数据，如果投影在一个1000面的物体上，那这个投影后，会多1000面，所以投影会直接增加屏幕上三角面的数量

##### 投影矩阵
- 主要有两个重要的矩阵
    - `float4x4 unity_Projector;`
    - `float4x4 unity_ProjectorClip;`
- 在顶点计算只，通过 `mul(unity_Projector,v.vertex);` 和 `mul(unity_ProjectorClip,v.vertex);` 来得到uv坐标
- 在片元着色器中，通过 tex2Dproj(贴图,uv) 来得到颜色值
    ```
    fixed4 frag (v2f i) : SV_Target
    {
        fixed4 col = tex2Dproj(_MainTex, i.uv);
        fixed4 falloff = tex2Dproj(_FallOff, i.uv2);
        return col * falloff.r;
    }
    ```

##### 注意
- 贴图的格式 是`Repeat`还是`Clamp` ，直接决定了最后的效果.
- Shader中的`Blend`可以修改投影的效果
- 如果`tex2Dproj`里面的`uv`是屏幕坐标，则在取uv的时候，需要使用`UNITY_PROJ_COORD(i.uv)`

##### 实验结果：
第一张图为投影到某个物体上，第二张图为用屏幕坐标来计算投影
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Projector/pic1.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Projector/pic2.png)