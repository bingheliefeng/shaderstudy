## 高光
	本章节主要学习Blinn-Phong算法
	算法可参考 Lighting.cginc 中的 UnityBlinnPhongLight 方法

#### 重点：
1. 计算 ViewDir
	- `float3 V = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);` 
		- 其中 `i.worldPos` 在顶点计算中 ，通过 `mul(unity_ObjectToWorld, v.vertex)` 来转化得到
		- `_WorldSpaceCameraPos` 是 内置变量
	- `float3 H = normalize(L+V);` 求出光照方向和相机方向的半向量，这是一步对 reflect的简化
	- `fixed3 spec = _LightColor0.rgb * _SpecColor.rgb * pow(saturate(dot(H,N)),_Specular*128.0);` 通过对反射光线和法线求点积算出此处的高光强度
	- 最后将 `环境光 + 漫反射 + 高光` 得到Blinn-Phong结果

#### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E9%AB%98%E5%85%89/pic.png)