## Billboard
	模型朝向相机
	主要有两种，一种是模型z会一直朝向相机，另一种是只是水平方向朝向相机，而竖直方向不会

##### 第一种：
- Billboard1 会一直朝向相机
```
float3 offset = float3(0,0.2,0);
float3 vpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz+offset);
float4 worldCoord = float4(unity_ObjectToWorld._m03, unity_ObjectToWorld._m13, unity_ObjectToWorld._m23, 1);
float4 viewPos = mul(UNITY_MATRIX_V, worldCoord) + float4(vpos, 0);
float4 worldPos = mul(UNITY_MATRIX_P, viewPos);
```

##### 第二种
- Billboard2 只是水平朝向，主要用于制作草和树
- 算法主要是求出物体和相机的向量，再计算朝向
```
fixed3 center = float3(0,0,0);
//物体空间原点
//将相机位置转换至物体空间并计算相对原点朝向，物体旋转后的法向将与之平行，这里实现的是Viewpoint-oriented Billboard
float3 viewer = mul(unity_WorldToObject,float4(_WorldSpaceCameraPos, 1));
float3 normalDir = viewer - center;
// _VerticalBillboarding为0到1，控制物体法线朝向向上的限制，实现Axial Billboard到World-Oriented Billboard的变换
normalDir.y =normalDir.y * _VerticalBillboarding;
normalDir = normalize(normalDir);
//若原物体法线已经朝向上，这up为z轴正方向，否者默认为y轴正方向
float3 upDir = abs(normalDir.y) > 0.999 ? float3(0, 0, 1) : float3(0, 1, 0);
//利用初步的upDir计算righDir，并以此重建准确的upDir，达到新建转向后坐标系的目的
float3 rightDir = normalize(cross(upDir, normalDir));     
upDir = normalize(cross(normalDir, rightDir));
// 计算原物体各顶点相对位移，并加到新的坐标系上
float3 centerOffs = v.vertex.xyz - center;
float3 localPos = center + rightDir * centerOffs.x + upDir * centerOffs.y + normalDir * centerOffs.z;
localPos.y += center.y;
```