﻿//树摇摆
Shader "Study/Tree"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Noise("Noise", 2D) = "black" {}
		_VerticalBillboarding("VerticalBillboarding",Range(0,1)) = 0

		_TimeScale ("Time Scale", Range(0,10)) = 1 //摇摆速度
        _WaveScale("Wave Scale",Range(0,1)) = 0.1 //摇摆幅度
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        ZWrite Off
        Cull Front
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _Noise;

            fixed _VerticalBillboarding;
            half _TimeScale;
            fixed _WaveScale;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                //=========摇摆=============
                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                fixed waveSample = tex2Dlod(_Noise, worldPos).r;
                float4 offset = float4(0,0,0,0);
				offset.x = sin( _TimeScale * waveSample * 3.1416 * _Time.y * clamp(v.vertex.y, 0, 1))  * _WaveScale;

                //如果不想用 _Noise ，可以直接用世界坐标来作为 wave参数
                // offset.x = sin( _TimeScale * normalize(worldPos) * 3.1416 * _Time.y * clamp(v.vertex.y, 0, 1))  * _WaveScale;


                //========Verticle billboard=========
				fixed3 center = offset;
				//物体空间原点
				//将相机位置转换至物体空间并计算相对原点朝向，物体旋转后的法向将与之平行，这里实现的是Viewpoint-oriented Billboard
				float3 viewer = mul(unity_WorldToObject,float4(_WorldSpaceCameraPos, 1));
				float3 normalDir = viewer - center;
				// _VerticalBillboarding为0到1，控制物体法线朝向向上的限制，实现Axial Billboard到World-Oriented Billboard的变换
				normalDir.y =normalDir.y * _VerticalBillboarding;
				normalDir = normalize(normalDir);
				//若原物体法线已经朝向上，这up为z轴正方向，否者默认为y轴正方向
				float3 upDir = abs(normalDir.y) > 0.999 ? float3(0, 0, 1) : float3(0, 1, 0);
				//利用初步的upDir计算righDir，并以此重建准确的upDir，达到新建转向后坐标系的目的
				float3 rightDir = normalize(cross(upDir, normalDir));     
				upDir = normalize(cross(normalDir, rightDir));
				// 计算原物体各顶点相对位移，并加到新的坐标系上
				float3 centerOffs = v.vertex.xyz - center;
				float3 localPos = center + rightDir * centerOffs.x + upDir * centerOffs.y + normalDir * centerOffs.z;
                localPos.y += offset.y;

                //==========
				o.vertex = UnityObjectToClipPos(float4(localPos, 1));
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
