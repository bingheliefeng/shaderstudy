## 消融
	模型融解

##### 关键代码
```
//_LineWidth宽度范围模拟渐变。t为0是正常颜色，为1时位于消融的边界
fixed t = 1 - smoothstep(0.0, _LineWidth, burn.r - _BurnAmount);
fixed3 burnColor = lerp(_BurnFirstColor, _BurnSecondColor, t);  //两个烧焦颜色做插值
burnColor = pow(burnColor, 5);  // 加强颜色模拟烧焦痕迹
```
- `burn` 是一张灰白图
- `_BurnAmount` 控制消融进度

##### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E6%B6%88%E8%9E%8D/pic.png)