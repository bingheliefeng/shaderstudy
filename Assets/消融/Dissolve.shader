﻿Shader "Study/Dissolve"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white"{}                        // 漫反射纹理
        _BurnMap("Burn Map", 2D) = "white"{}                            // 噪声纹理
        _BurnAmount ("Burn Amount", Range(0.0, 1.0)) = 0.0              // 消融程度 0为正常效果
        _LineWidth("Burn Line Width", Range(0.0, 0.2)) = 0.1            // 消融效果时的线宽
        _BurnFirstColor("Burn First Color", Color) = (1, 0, 0, 1)       // 火焰边缘颜色值
        _BurnSecondColor("Burn Second Color", Color) = (1, 0, 0, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 N : NORMAL;
                float3 L : TEXCOORD2;
                float3 V : TEXCOORD3;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _BurnMap;
            float4 _BurnMap_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.uv, _BurnMap);
                UNITY_TRANSFER_FOG(o,o.vertex);

                o.N = v.normal;
                o.L = ObjSpaceLightDir(o.vertex);
                o.V = ObjSpaceViewDir(o.vertex);
                return o;
            }

            fixed _BurnAmount;
            fixed4 _BurnFirstColor;
            fixed4 _BurnSecondColor;
            fixed _LineWidth;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 burn = tex2D(_BurnMap,i.uv.zw);
                clip(burn.r - _BurnAmount);     // 如果小于0，该像素会被剔除

                //_LineWidth宽度范围模拟渐变。t为0是正常颜色，为1时位于消融的边界
                fixed t = 1 - smoothstep(0.0, _LineWidth, burn.r - _BurnAmount);
                fixed3 burnColor = lerp(_BurnFirstColor, _BurnSecondColor, t);  //两个烧焦颜色做插值
                burnColor = pow(burnColor, 5);  // 加强颜色模拟烧焦痕迹

                //光照相关计算
                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);
                fixed3 V = normalize(i.V);
                fixed3 H = normalize(L+V);

                fixed3 diff = _LightColor0.rgb * (dot(L,N) * 0.5 + 0.5 );
                fixed3 spec = _LightColor0.rgb * pow(saturate(dot(N,H)),0.3*128) * 2;

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv.xy);
                col.rgb *= diff + UNITY_LIGHTMODEL_AMBIENT.rgb + spec;

                //将消融结果叠加
                col.rgb = lerp(col.rgb, burnColor, t * step(0.0001, _BurnAmount));

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
