﻿Shader "Study/Fresnel"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Spec("Spec",Range(0,1)) = 0.5
        _FresnelScale("Fresnel Scale",Range(0,1)) = 0.7
        _FresnelPower("Fresnel power",Range(0,5)) =  2
        _FresnelCol ("Fresnel Color",Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 n:NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 N:NORMAL;
                float3 worldPos:TEXCOORD1;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.N = UnityObjectToWorldNormal(v.n); //WorldNormal
                o.worldPos = mul(unity_ObjectToWorld,v.vertex); //世界坐标

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            half _FresnelPower;
            fixed _Spec;
            fixed _FresnelScale;
            fixed4 _FresnelCol;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 N = normalize(i.N);
                float3 V = normalize(_WorldSpaceCameraPos.xyz-i.worldPos);
                float3 L = normalize(_WorldSpaceLightPos0.xyz);

                float3 diff = _LightColor0.rgb * (dot(N,L)*0.5+0.5);

                float3 H = normalize(L+V);
                float3 spec = _LightColor0.rgb * _FresnelScale * pow(max(0,dot(N,H)),_Spec*128.0);

                float fresnel = _FresnelScale * pow(1.0 - max(0,dot(N,V)),_FresnelPower);

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *= diff + spec + UNITY_LIGHTMODEL_AMBIENT.rgb;
                col.rgb = lerp( col.rgb , _FresnelCol , fresnel);
        
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
