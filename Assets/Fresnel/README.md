## Fresnel
	此算法的目的是主要是用于镜面反射，例如水面，越远越亮
	但是也可以用于角色或汽车的一些边缘光效，边缘区域高亮一些
	此算法和高光算法有一点相似，只是pow里面用了一个`1-`

#### 算法
- `float fresnel = _FresnelScale * pow(1.0 - saturate(dot(N,V)),_FresnelPower);`
	- `_FresnelScale` 在0-1之间
	- `_FresnelPower` 在0-5之间

#### 重点：
- 需要先求出 ViewDir 和 WorldNormal
	- `ViewDir` 通过 `normalize(_WorldSpaceCameraPos.xyz-i.worldPos)` 得到
	- `WorldNormal` 通过在顶点计算中用 `ObjectToWorldNormal(v.vertex)` 得到，然后在fragment着色器中 归一化处理
	- 求出fresnel值后，再进行最后的颜色叠加 `col.rgb = lerp(col.rgb , _FresnelCol , fresnel);`

#### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Fresnel/pic.png)