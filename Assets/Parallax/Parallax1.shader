﻿Shader "Study/Parallax1"
{
    Properties
    {
        _Atten("Atten",Range(1,2))=1
        _MainTex ("Texture", 2D) = "white" {}
        _NormalTex ("Normal Texture", 2D) = "white" {}
        _ParallaxTex ("Parallax Texture", 2D) = "white" {}
        _Height("Height",Range(-0.1,0.1)) = 0.05

        [Header(Fresnel)]
        _FresnelScale("Fresnel Scale",Range(0,2)) = 1
        _FresnelPower("Fresnel Power",Range(1,5)) = 1
        _FresnelColor("Fresnel Color",Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal:NORMAL;
                float4 tangent:TANGENT;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float3 L:TEXCOORD2;
                float3 V:TEXCOORD3;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _ParallaxTex;
            sampler2D _NormalTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                TANGENT_SPACE_ROTATION;
                o.L = mul((float3x3)rotation,ObjSpaceLightDir(v.vertex));
                o.V = mul((float3x3)rotation,ObjSpaceViewDir(v.vertex));

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed _Height;
            fixed _Atten;

            fixed4 _FresnelColor;
            half _FresnelPower;
            half _FresnelScale;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 L = normalize(i.L);
                float3 V = normalize(i.V);

                fixed h = tex2D(_ParallaxTex,i.uv).r;
                half2 offset = ParallaxOffset(h,_Height,V);
                i.uv += offset;

                float3 N = normalize(UnpackNormal(tex2D(_NormalTex,i.uv)));
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                fixed3 diff = _LightColor0.rgb * ( dot(N,L) * 0.5 + 0.5 );
                fixed3 spec = _LightColor0.rgb * pow( saturate(dot(N,normalize(L+V))) , 128 );
                fixed fresnel = _FresnelScale * pow( 1- saturate(dot(N,V)) , _FresnelPower );

                col.rgb *= (diff + UNITY_LIGHTMODEL_AMBIENT.rgb + spec) * _Atten;

                col.rgb = lerp(col.rgb,_FresnelColor,fresnel);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
