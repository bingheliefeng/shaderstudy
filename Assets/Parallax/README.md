## 凹凸贴图
	通过贴图来实现凹凸有几种，一种是用视差贴图偏移uv来欺骗眼睛，一种是法线贴图加光照来达到凹凸感，另一种是地势Relief算法

	下面的demo主要是用的 视差贴图 方式

##### 要点:
- 在切空间中计算
```
TANGENT_SPACE_ROTATION;
o.L = mul((float3x3)rotation,ObjSpaceLightDir(v.vertex));
o.V = mul((float3x3)rotation,ObjSpaceViewDir(v.vertex));
```
- 视差贴图是黑白图，越白越高
```
float3 L = normalize(i.L);
float3 V = normalize(i.V);
//求出高度，进行uv偏移
fixed h = tex2D(_ParallaxTex,i.uv).r;
half2 offset = ParallaxOffset(h,_Height,V);
i.uv += offset;
```

##### 注意：
	Shader中如果用了法线贴图，或者要处理凹凸，最好是在切空间中计算，主要是法线贴图一般是用的切空间的法线贴图


#### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Parallax/pic.png)