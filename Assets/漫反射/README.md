## 漫反射
	本章节主要学习Lambert算法

- 漫反射 之 顶点光照
	- 光照计算在顶点着色器中
- 漫反射 之 像素光照
	- 光照计算在片元着色器中
- 半Lambert
	- 让暗部没那么暗

#### 重点：
1. 法线需要转到 世界坐标系
	使用 `UnityObjectToWorldNormal(v.normal)`来转换到世界坐标系，不需要再执行归一化，这个方法已经包含了。
2. 需要加环境光 `UNITY_LIGHTMODEL_AMBIENT.rgb` 值
3. Lamber 计算公式为 
	`UNITY_LIGHTMODEL_AMBIENT.rgb + saturate(dot(worldNormal,_WorldSpaceLightPos0.xyz)) * _LightColor0.rgb`
	- `_WorldSpaceLightPos.xyz` 是指第一个直线光的光照方向
	- `_LightColor0.rgb` 是指第一个直线光的光照颜色
	- 注意要引入`Lighting.cginc`
4. Half Lambert 算法
	在计算漫反射时，`saturate(dot(worldNormal,_WorldSpaceLightPos0.xyz))` 这部分算法改成 `dot(worldNormal,_WorldSpaceLightPos0.xyz)*0.5+0.5`
	这样就可以让光照强度至少有0.5的值，使其不会太暗。

#### 实验结果：
	顶点光照性能高，但是效果差一些。
	半Lambert可以让画面过滤更自然
	下图分别是顶点光，像素光，半Lambert像素光

![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E6%BC%AB%E5%8F%8D%E5%B0%84/pic1.png)