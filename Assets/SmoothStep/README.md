## SmoothStep(a,b,x)
	获得从0-1的平滑曲线
	具体可参考 https://zhuanlan.zhihu.com/p/157758600
- 如果a>b,则曲线是*从高到低*，则x大于a为0，小于b为1
	- 例如smoothstep(0.5,0.3,x)，x大于0.5时为0，小于0.3时为1，在0.3-0.5之间为0-1的过渡
- 如果b>a,则曲线是*从低到高*，则x小于a时为0，大于b时为1
	- 例如smoothstep(0.3,0.5,x)，x小于0.3时为0，大于0.5时为1

##### 实验结果：
图中上部分两张图分别为 `smoothstep(0.5,0.3,x)` 和 `smoothstep(0.3,0.5,x)`
图中上部分的圆环图为 `smoothstep(0.2, 0.3, x) - smoothstep(0.3, 0.4, x)`
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/SmoothStep/pic.png)