﻿Shader "study/SmoothStep1"
{
    Properties
    {
        _Val1("Step Value 1",Float) = 0.5
        _Val2("Step Value 2",Float) = 0.2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            half _Val1;
            half _Val2;

            fixed4 frag (v2f i) : SV_Target
            {
                return smoothstep(_Val1, _Val2, length(i.uv - 0.5));
            }
            ENDCG
        }
    }
}
