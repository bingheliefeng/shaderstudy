﻿Shader "Study/Toon/Toon1"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Steps("Steps of toon",Range(0,9)) = 3
        _ToonEffect("Toon Effect",Range(0,1)) = 1

        [Header(Specular)]
        _SpecularScale("Specular Scale",Range(0.001,0.2)) = 0.1
        _SpecularPower("Specular Power",Range(0.0,1.0)) = 0.5

        [Header(Outline)]
        _Outline("Outline",Range(0,2)) = 1
        _OutlineFactor("Outline Factor",Range(0,1)) = 0.5
        _OutColor("Outline Color",Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            Cull Front
            ZWrite On

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float4 pos : SV_POSITION;
            };

            fixed _Outline;
            fixed _OutlineFactor;

            v2f vert(appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);

                float3 vertex = normalize(v.vertex);
                float3 normal = normalize(v.normal);
                float3 nv = lerp(vertex,normal,_OutlineFactor);
                float3 dir = mul((float3x3)UNITY_MATRIX_IT_MV, nv);

                //转到投影空间
                float2 projPos = normalize( TransformViewToProjection(dir.xy) );
                //描边
                o.pos.xy += projPos * o.pos.z * _Outline;

                return o;
            }

            fixed4 _OutColor;
            fixed4 frag(v2f i):SV_Target
            {
                return _OutColor;
            }
            ENDCG
        }


        Pass
        {
            Tags { "LightMode"="ForwardBase" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 N : NORMAL;
                float3 L : TEXCOORD1;
                float3 V : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.N = v.normal;
                o.L = ObjSpaceLightDir(v.vertex);
                o.V = ObjSpaceViewDir(v.vertex);
                return o;
            }

            half _Steps;
            fixed _ToonEffect;
            fixed _SpecularScale;
            fixed _SpecularPower;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);
                fixed3 V = normalize(i.V);
                fixed3 H = normalize(L+V);

                //卡通高光
                fixed spec = saturate(dot(N,H));
                fixed w = fwidth(spec) * 2.0; //取高光区域抗锯齿操作，fwidth：邻域像素的近似导数。
                fixed4 specular = _LightColor0 * lerp(0, 1, smoothstep(-w, w, spec + _SpecularScale - 1)) * _SpecularPower;

                fixed diff = (dot(L,N)*0.5+0.5);
                float toon = floor(diff * _Steps)/_Steps;//也可以用round
                diff = lerp(diff,toon,_ToonEffect);

                fixed4 col = tex2D(_MainTex, i.uv);
                col *= (_LightColor0 * diff + UNITY_LIGHTMODEL_AMBIENT + specular);
              
                return col;
            }
            ENDCG
        }
    }
}
