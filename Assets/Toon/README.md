## Toon 卡通着色

##### 两种方式
- 第一种：使用`floor或round`对漫反射颜色分层
	```
	fixed diff = (dot(L,N)*0.5+0.5);
	float toon = floor(diff * _Steps)/_Steps;//也可以用round
	diff = lerp(diff,toon,_ToonEffect);
	```
- 第二种：使用一张有不同图片的图片来处理
	- 上面的代码就变成了对这张图片进行采样，但是采样用的是diff参数
	```
	fixed diff = (dot(L,N)*0.5+0.5);
	fixed3 stepCol = tex2D(_StepTex,fixed2(diff,diff));
	```
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Toon/toon.png)

##### 卡通着色高光处理
```
fixed spec = saturate(dot(N,H));
fixed w = fwidth(spec) * 2.0; //取高光区域抗锯齿操作，fwidth：邻域像素的近似导数。
fixed4 specular = _LightColor0 * lerp(0, 1, smoothstep(-w, w, spec + _SpecularScale - 1)) * _SpecularPower;
```

##### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Toon/pic.png)