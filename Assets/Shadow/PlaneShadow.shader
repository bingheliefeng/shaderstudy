﻿//在一个平面上产生阴影
Shader "Study/Shadow/Plane Shadow"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [Header(Shadow)]
        _ShadowColor("Shadow Color",Color) = (0,0,0,0.5)
        _ShadowFalloff("Shadow Falloff",Range(0,1))=0
        _ShadowY("Shadow Y",Float) = 0
    }
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase  // 非常重要，不然没法正确计算阴影

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                SHADOW_COORDS(2) 
                float4 pos : SV_POSITION; //阴影计算时需要这个 pos

                float3 N : NORMAL;
                float3 L : TEXCOORD3;
                float3 V : TEXCOORD4;

                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                float3 worldPos = mul(unity_ObjectToWorld,v.vertex);
                o.L = UnityWorldSpaceLightDir(worldPos);
                o.V = UnityWorldSpaceViewDir(worldPos);
                o.N = UnityObjectToWorldNormal(v.normal);

                UNITY_TRANSFER_FOG(o,o.vertex);
                TRANSFER_SHADOW(o)  // 填充阴影坐标，根据平台自动调整
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //shadow
                fixed shadow = SHADOW_ATTENUATION(i) * 0.5 + 0.5;

                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);
                fixed3 V = normalize(i.V);
                fixed3 H = normalize(L+V);
                fixed3 diff = _LightColor0.rgb * ( dot(L,N) * 0.5 + 0.5 ) * shadow;
                fixed3 spec = _LightColor0.rgb * pow ( saturate( dot(N,H) ) , 128 ) * shadow ;

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *=  UNITY_LIGHTMODEL_AMBIENT.rgb + diff + spec ;

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }


        Pass
        {
            Name "Shadow"

            Tags { "RenderType"="Transparent" "LightMode"="Forwardbase"}

            Stencil
            {
                Ref 0
                Comp equal
                Pass incrWrap
                Fail keep
                ZFail keep
            }

            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            Cull back
            Offset -1, -1

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing
            #pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                fixed4 color : COLOR;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            float4 _ShadowColor;
            fixed _ShadowFalloff;
            float _ShadowY;

            v2f vert(appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                float3 worldPos = mul(unity_ObjectToWorld, v.vertex)/v.vertex.w;
                float3 lightDir = UnityWorldSpaceLightDir(worldPos);

                _ShadowY = min(worldPos.y , _ShadowY);
                float3 shadowPos = float3(0,_ShadowY,0);
                shadowPos.xz = worldPos.xz - lightDir.xz * worldPos.y / lightDir.y;

                float3 center = float3(unity_ObjectToWorld[0].w, shadowPos.y , unity_ObjectToWorld[2].w);
                fixed falloff = 1 - saturate( distance(shadowPos, center) * _ShadowFalloff );

                o.color = _ShadowColor * falloff;

                o.pos = UnityWorldToClipPos(shadowPos);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
    }
}