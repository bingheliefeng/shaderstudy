﻿using UnityEngine;

public class ProjectorShadow : MonoBehaviour
{

    public Projector proj;
    public Camera projCam;
    public Shader ShadowColor;
    private RenderTexture _rt;

    void Start()
    {
        projCam.enabled = false;
        proj.enabled = true;

        _rt = new RenderTexture(1024,1024,0,RenderTextureFormat.R8);
        _rt.wrapMode = TextureWrapMode.Clamp;
        _rt.filterMode = FilterMode.Bilinear;
        _rt.autoGenerateMips = false;
    
        projCam.targetTexture = _rt;
        proj.material.SetTexture("_ShadowTex",_rt);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (proj && projCam)
        {
            projCam.enabled = true;
            projCam.aspect = proj.aspectRatio;
            projCam.orthographicSize = proj.orthographicSize;
            projCam.RenderWithShader(ShadowColor, null);
            projCam.enabled = false;
        }

    }
}
