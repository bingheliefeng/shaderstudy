﻿Shader "Study/Shadow/Projector Shadow"
 {
     Properties
     {
         _Strength ("Shadow Strength",Range(0,1)) = 1
         [HideInInspector]_ShadowTex ("Cookie", 2D) = "gray"
         _FalloffTex ("FallOff", 2D) = "white"
     }
      
     Subshader
     {
         Tags { "RenderType"="Transparent" }
         Pass
         {
	        ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			Offset -1, -1
                          
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma fragmentoption ARB_precision_hint_fastest
             #include "UnityCG.cginc"
              
             struct v2f
             {
                 float4 pos : SV_POSITION;
                 float4 uv_Main : TEXCOORD0;
                 float4 uv_Clip : TEXCOORD1;
             };
             
              
             sampler2D _ShadowTex;
             sampler2D _FalloffTex;
             float4x4 unity_Projector;
             float4x4 unity_ProjectorClip;
              
             v2f vert(appdata_tan v)
             {
                 v2f o;
                 o.pos = UnityObjectToClipPos (v.vertex);
                 o.uv_Main = mul (unity_Projector, v.vertex);
                 o.uv_Clip = mul (unity_ProjectorClip, v.vertex);
                 return o;
             }

             fixed _Strength;
              
             half4 frag (v2f i) : COLOR
             {
                half4 tex = tex2Dproj(_ShadowTex, i.uv_Main);
                tex.a = step(tex.r,0);

                //边界处理
                half2 shadowUV = i.uv_Main.xy/i.uv_Main.w ;
                tex.a *= step(0.01,shadowUV.x);
                tex.a *= step(0.01,shadowUV.y);
                tex.a *= step(shadowUV.x,0.99);
                tex.a *= step(shadowUV.y,0.99);

                half falloff = tex2Dproj(_FalloffTex, i.uv_Clip).r;
                tex.a *= falloff;
                tex.a *= _Strength;
                 return tex;
             }
             ENDCG
      
         }
     }
 }