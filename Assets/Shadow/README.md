## Shadow

##### Shadow Mapping
- 这是Unity默认的阴影方式，这种方式功能最强，能在台阶等这种不水平的物体上显示阴影，缺点是性能比较差，要达到好的效果，计算量很大，性能差。
- 要实现阴影，计算者和接收者的Shader中都需要加入相应的代码。
- ShadowMap技术是从灯光空间用相机渲染一张RenderTexture，把深度值写入其中所以称之为深度图，在把接受阴影的物体从模型空间转换到灯光空间中，获取深度图里的深度进行比较，如果深度值比深度图中取出的值大就说明该点为阴影。
- 其中生成阴影需要一个单独的 pass
```
Pass
{
	Tags { "LightMode" = "ShadowCaster" }

	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#pragma multi_compile_shadowcaster
	#include "UnityCG.cginc"

	struct v2f {
		V2F_SHADOW_CASTER;
		UNITY_VERTEX_OUTPUT_STEREO
	};

	v2f vert( appdata_base v )
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
		TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
		return o;
	}

	float4 frag( v2f i ) : SV_Target
	{
		SHADOW_CASTER_FRAGMENT(i)
	}
	ENDCG
}
```
- 接收阴影和雾的计算比较类似
	1. 添加编译参数`#pragma multi_compile_fwdbase`
	2. 在Tags中添加`"LightMode"="Forwardbase"`
	3. v2f 中添加`SHADOW_COORDS`，如下
	```
	struct v2f
	{
		float2 uv : TEXCOORD0;
		UNITY_FOG_COORDS(1)
		SHADOW_COORDS(2) 
		float4 pos : SV_POSITION; //阴影计算时需要这个 pos 变量
	};
	```
	4. 然后在顶点计算中，需要加入`TRANSFER_SHADOW(o)`
	5. 在片元着色器中获取阴影值`fixed shadow = SHADOW_ATTENUATION(i) * 0.5 + 0.5 ;`
	6. 最后将阴影值叠加到 Diffuse 和 Specular 上

- Shadow Mapping 其他一些参数
	- 阴影投射 (Shadow Projection)：从平行光源投射阴影有两种方法。紧密配合 (Close Fit) 渲染分辨率更高的阴影，但是如果摄影机移动，这些阴影有时就会有些许摇晃。
	- 稳定配合 (Stable Fit)：渲染分辨率更低的阴影，而阴影不会随摄影机的移动而摇晃。
	- 阴影距离 (Shadow Distance)：从摄影机处可以看见阴影的最大距离。超出此距离的阴影将不会被渲染。
	- 阴影层叠 (Shadow Cascades)：可设置为零、二或四。层叠数越高质量越好，但这要以处理开销为代价。


##### Plane Shadow
- 平面阴影，阴影计算Pass中将模型沿光照方向压扁到水平面上（即只保留世界坐标中的xz方向的值）
- 这种阴影不需要接收者，所以只适合于地面是完全平的
- 这种阴影性能高，不需要在光照方向生成深度图
- 注意：如果要获取光照阴影强度`_LightShadowData.x`的值，需要加`"LightMode"="Forwardbase"`和`#pragma multi_compile_fwdbase`
```
float3 worldPos = mul(unity_ObjectToWorld, v.vertex)/v.vertex.w;
float3 lightDir = UnityWorldSpaceLightDir(worldPos);

float3 shadowPos = float3(0,0,0);
shadowPos.xz = worldPos.xz - lightDir.xz * worldPos.y / lightDir.y;

float3 center = float3(unity_ObjectToWorld[0].w, shadowPos.y , unity_ObjectToWorld[2].w);
fixed falloff = 1 - saturate( distance(shadowPos, center) * _ShadowFalloff );

o.color = _ShadowColor * falloff;
```

##### Projector Shadow
- 这种是通过另一个相机渲染，生成一张纯色的RenderTexture图
- 再通过投影方式把阴影投到地面上
- 这种方式虽然有点麻烦，但是阴影计算者和阴影接收者Shader都不需要怎么添加代码就可以处理，代码基本都在投影上。重点就是分好层级，决定哪些层是阴影计算层，哪些层是阴影显示的层。
- 但是这种方式，如果角色和模型有穿插，则投影会有问题
- 步骤：
	- 创建一个GameObject，添加Projector组件和Camera组件，调整其Layer参数，投影组件的Layer一般只需要支持地面就可以，而Camera一般只渲染角色
	- 创建CSharp脚本，里面的逻辑主要是创建一个R8格式的RenderTexture，把脚本挂在上一步的GameObject上，脚本中主要是对相机设置Rendertexture，让其渲染出投影图
	- 当然投影图只需要纯色就可以，所以创建一个纯色的Shader，在上一步的Camera渲染里，调用Camera.RenderWidthShader来渲染出阴影图
	- Projector上面需要一个投影材质，所以需要创建另一个Shader和Material来显示投影
	```
	sampler2D _ShadowTex;
	sampler2D _FalloffTex;
	float4x4 unity_Projector;
	float4x4 unity_ProjectorClip;
	
	v2f vert(appdata_tan v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos (v.vertex);
		o.uv_Main = mul (unity_Projector, v.vertex);
		o.uv_Clip = mul (unity_ProjectorClip, v.vertex);
		return o;
	}

	fixed _Strength;
	
	half4 frag (v2f i) : COLOR
	{
		half4 tex = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.uv_Main));
		tex.a = step(tex.r,0);

		//边界处理
		half2 shadowUV = i.uv_Main.xy/i.uv_Main.w ;
		tex.a *= step(0.01,shadowUV.x);
		tex.a *= step(0.01,shadowUV.y);
		tex.a *= step(shadowUV.x,0.99);
		tex.a *= step(shadowUV.y,0.99);

		half falloff = tex2Dproj(_FalloffTex, UNITY_PROJ_COORD(i.uv_Clip )).r;
		tex.a *= falloff;
		tex.a *= _Strength;
		return tex;
	}
	```

##### 实验结果：
第一张为Shadow Map
第二张为Plane Shadow
第三张为Projector Shadow
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Shadow/pic1.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Shadow/pic2.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Shadow/pic3.png)