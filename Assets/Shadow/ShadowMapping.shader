﻿Shader "Study/Shadow/Shadow Mappping"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_instancing
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile_fwdbase  // 非常重要，不然没法正确计算阴影
            #pragma skip_variants VERTEXLIGHT_ON LIGHTPROBE_SH

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
				UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                SHADOW_COORDS(2) 
                float4 pos : SV_POSITION; //阴影计算时需要这个 pos

                float3 N : NORMAL;
                float3 L : TEXCOORD3;
                float3 V : TEXCOORD4;

                UNITY_VERTEX_OUTPUT_STEREO
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.V = ObjSpaceViewDir(v.vertex);
                o.L = ObjSpaceLightDir(v.vertex);
                o.N = v.normal;

                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_SHADOW(o)  // 填充阴影坐标，根据平台自动调整
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //shadow
                fixed shadow = SHADOW_ATTENUATION(i) * 0.5 + 0.5;

                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);
                fixed3 V = normalize(i.V);
                fixed3 H = normalize(L+V);
                fixed3 diff = _LightColor0.rgb * ( dot(L,N) * 0.5 + 0.5 );
                fixed3 spec = _LightColor0.rgb * pow ( saturate( dot(N,H) ) , 128 ) ;

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *=  UNITY_LIGHTMODEL_AMBIENT.rgb + diff + spec ;
                col.rgb *= shadow;
                
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }

        //计算阴影，用于在光照空间生成阴影深度度时用
        Pass
        {
            Tags { "LightMode" = "ShadowCaster" }
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
			#pragma multi_compile_instancing
            #pragma skip_variants SHADOWS_CUBE SHADOWS_DEPTH
            
            #include "UnityCG.cginc"
        
            struct v2f {
                V2F_SHADOW_CASTER;
                UNITY_VERTEX_OUTPUT_STEREO
            };
        
            v2f vert( appdata_base v )
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                return o;
            }
        
            float4 frag( v2f i ) : SV_Target
            {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
}
