﻿Shader "Study/Shadow/Shadow Color"
{
	Properties   
    {  
        _ShadowColor ("Main Color", COLOR) = (0,0,0,1)  
    }
 
	SubShader
	{
		Pass   
        {  
            Color [_ShadowColor]
        } 
	}
}