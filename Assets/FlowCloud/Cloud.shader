﻿Shader "Study/Cloud"
{
    Properties
    {
        [Header(Layer1)]
        _Layer1Tex ("Layer1 Texture", 2D) = "white" {}
        _Layer1ScrollX("Layer1 ScrollX",Range(0,2)) = 0
        _Layer1ScrollY("Layer1 ScrollY",Range(0,2)) = 0
        _Layer1Power("Layer1 Power",Float) = 2

        [Header(Layer2)]
        _Layer2Tex ("Layer2 Texture", 2D) = "white" {}
        _Layer2ScrollX("Layer2 ScrollX",Range(0,2)) = 0
        _Layer2ScrollY("Layer2 ScrollY",Range(0,2)) = 0
        _Layer2Power("Layer2 Power",Float) = 1

        [Header(Mask)]
        _MaskTex("Mask Texture",2D) = "white"{}
        _MaskPower("Mask Power",Range(0,2)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent+10" }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            // #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float2 mk_uv : TEXCOORD1;
                // UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _Layer1Tex;
            float4 _Layer1Tex_ST;

            sampler2D _Layer2Tex;
            float4 _Layer2Tex_ST;

            sampler2D _MaskTex;

            float _Layer1ScrollX;
            float _Layer1ScrollY;
            float _Layer2ScrollX;
            float _Layer2ScrollY;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _Layer1Tex) + float2(_Layer1ScrollX, _Layer2ScrollY) * _Time.x ;
                o.uv.zw = TRANSFORM_TEX(v.uv, _Layer2Tex) + float2(_Layer2ScrollX, _Layer2ScrollY) * _Time.x ;
                o.mk_uv = v.uv;
                // UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed _MaskPower;
            half _Layer1Power;
            half _Layer2Power;

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = 1;
                fixed4 col1 = tex2D(_Layer1Tex, i.uv.xy  ) * _Layer1Power;
                col.a *= col1.r;

                fixed4 col2 = tex2D(_Layer2Tex, i.uv.zw  ) * _Layer2Power;
                col.a *= col2.r;

                fixed4 mask = tex2D(_MaskTex , i.mk_uv);
                // apply fog
                // UNITY_APPLY_FOG(i.fogCoord, col);

                col.a *= pow(mask.r,_MaskPower+1); 

                return col;
            }
            ENDCG
        }
    }
}
