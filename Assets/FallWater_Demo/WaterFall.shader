﻿Shader "Unlit/WaterFall"
{
    Properties
    {
        _Color("Color",Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        _MaskTex ("Mask Tex", 2D) = "white" {}
        _DistorTex("Distor Tex",2D) = "white" {}
        _FallSpeed("Fall Speed", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;

                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _DistorTex;
		    float4 _DistorTex_ST;

            sampler2D _MaskTex;
            half _FallSpeed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);

                v.uv.x = normalize(mul(unity_ObjectToWorld,v.vertex)).x;//防止相同
                o.uv.zw = TRANSFORM_TEX(v.uv, _DistorTex) + float2(0,_FallSpeed*_Time.y) ;

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 _Color;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _Color;
                fixed4 mk = tex2D(_MaskTex,i.uv.xy);
                fixed4 distor = tex2D(_DistorTex,i.uv.zw);
                // sample the texture
                col.rgb += tex2D(_MainTex, i.uv.xy + distor.xy).r;
                col.a *= mk.r;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
