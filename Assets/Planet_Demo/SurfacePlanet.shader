﻿Shader "Custom/SurfacePlanet"
{
    Properties
    {
        _Color("Color",Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}

        _CloudTex("Cloud",2D) = "White"{}

        _BumpTex("Bump Tex",2D) = "white"{}
        _Specular("Specular",Range(0,1)) = 0.5
        _SpecColor("Specular Color",Color) = (1,1,1,1)
        _Gloss("Gloss",Range(0,1)) = 0.2 
        
        [Space]
        _FireTex("Fire Tex",2D) = "white"{}
        _Color2("Color2",Color)=(1,1,1,1)
        _Color3("Color3",Color)=(1,1,1,1)
        _Color4("Color4",Color)=(1,1,1,1)
 
        _Radius("PlanetRadius",Float) = 0.1
    }
    SubShader
    {
        Tags { "Queue"="Transparent"}
        Pass
        {
            Tags{"RenderType"="Transparent" }
            Blend SrcAlpha One
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 N : NORMAL;
                float3 V : TEXCOORD0;
            };

            v2f vert(appdata_base v)
            {
                v2f o;
                v.vertex.xyz += v.normal * 0.05;
                o.pos = UnityObjectToClipPos(v.vertex);
                
                o.N = v.normal;
                o.V = ObjSpaceViewDir(v.vertex);

                return o;
            }
            fixed4 frag(v2f i):SV_Target
            {
                fixed3 N = normalize(i.N);
                fixed3 V = normalize(i.V);
                fixed rim = pow(saturate(dot(N,V)),1);
                fixed4 c = 1;
                c *= rim;
                return c;
            }
            ENDCG
        }

        Tags { "RenderType"="Opaque"}
        CGPROGRAM
        #pragma surface surf MyBlinnPhong noforwardadd noshadow vertex:vert
        #pragma target 3.0
        
        sampler2D _MainTex;
        sampler2D _CloudTex;
        sampler2D _BumpTex;
        sampler2D _FireTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpTex;
            float2 uv_FireTex;

            float height;
        };

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.height = length(v.vertex);
        }

        inline fixed4 LightingMyBlinnPhong(SurfaceOutput o,fixed3 L,half3 V , fixed atten)
        {
            fixed3 N = o.Normal;
            fixed3 H = normalize(L+V);

            fixed4 col;
            col.rgb = o.Albedo;
            col.a = o.Alpha;
            //diffuse
            col.rgb *= _LightColor0.rgb * (dot(H,N) * 0.5 + 0.5) * atten * o.Gloss;

            //spec
            col.rgb += pow(saturate(dot(N,H)),128 * 0.3) * o.Specular * atten;

            fixed fresnel = pow( 1- saturate(dot(N,V)),3) * 0.75;
            col.rgb = lerp(col.rgb,fixed3(1,1,1),fresnel);

            return col ;
        }
        
        fixed3 _Color2;
        fixed3 _Color3;
        fixed3 _Color4;

        fixed _Gloss;
        fixed _Specular;

        fixed4 _Color;
        half _Radius;

        void surf (Input IN, inout SurfaceOutput o)
        {

            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            fixed4 fire = tex2D(_FireTex, IN.uv_FireTex);

            fixed fireBlend = pow(saturate(_Radius - IN.height) * 30, 0.5);

            fixed3 fireCol = 0;
            fireCol = lerp(fireCol, _Color2, saturate(fireBlend));
            fireCol = lerp(fireCol, _Color3 * 1.2, saturate(fireBlend-0.333));
            fireCol = lerp(fireCol, _Color4 * 1.5, saturate(fireBlend-0.666));
            
            fire.rbg *= fireCol * 5;
            c.rgb += fire ;

            o.Albedo = c.rgb ;
            o.Normal = UnpackNormal(tex2D(_BumpTex,IN.uv_BumpTex));
            o.Specular = _Specular;
            o.Gloss = _Gloss;

            fixed4 cloud = tex2D(_CloudTex,IN.uv_MainTex+float2(_Time.y*0.003,0));
            cloud.a *= saturate(1-fireBlend);
            o.Albedo = lerp(o.Albedo,cloud.rgb,cloud.a) ;

            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
