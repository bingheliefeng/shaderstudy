﻿using UnityEngine;

public class Planet : MonoBehaviour
{
    public float penScale = 0.3f;

    private Mesh _mesh;
    private Vector3[] _vs;

    [SerializeField]
    private float _radius = 1f;

    private bool _flag;
    // Start is called before the first frame update
    void Start()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Radius",_radius);
        _vs = _mesh.vertices;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DrawOnMesh();
        }
        if(_flag)
        {
            _flag = false;

            _mesh.vertices = _vs;
            // _mesh.RecalculateNormals();

            //更新mesh collider
            MeshCollider collider = gameObject.GetComponent<MeshCollider>();
            collider.sharedMesh = _mesh;
        }
    }

    void DrawOnMesh()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000,1<<LayerMask.NameToLayer("Default")))
        {
            float radius = _radius + 0.1f;
            Vector3 hitLocalP = transform.InverseTransformPoint(hit.point);
            Vector3 actingForcePoint = transform.InverseTransformPoint(hit.point + hit.normal);//发力点指向球的本地坐标向量
            for (int i = 0; i < _vs.Length; i++)
            {
                if (Vector3.Distance(_vs[i], Vector3.zero) < radius && Vector3.Distance(_vs[i], hitLocalP) < radius * penScale * 0.1f)
                {
                    Vector3 pointToVertex = _vs[i] - actingForcePoint;//作用力点指向当前顶点位置的向量
                    float actingForce = 0.02f / pointToVertex.sqrMagnitude;//作用力大小
                    _vs[i] += pointToVertex.normalized * actingForce * Time.deltaTime;//顶点速度向量
                }
            }
            _flag = true;
        }
    }

}
