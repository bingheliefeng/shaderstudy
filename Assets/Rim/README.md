## Rim
	模型发光，关键是算出ViewDir和Normal
	但是这种方式的发光有一定的弊端，面数少的模型可能效果不太好

##### 内发光
	内发光的算法和Fresnel类似，只是不需要用`pow(1-saturate(dot(V,N)),3)`，只需要`pow(saturate(dot(V,N)),0.3)`

##### 外发光
	需要多一个`Pass`，这个`Pass`的作用主要用于扩展模型
	扩展模型后，再执行内发光的算法，求得发光的Rim值，然后对其alpha进行处理

##### 实验结果：
下图中有内发光和外发光
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Rim/pic.png)