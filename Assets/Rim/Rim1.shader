﻿Shader "Study/Rim/Rim1"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NormalTex ("Normal Texture", 2D) = "white" {}
        _NormalScale("Normal Scale",Range(0,1)) = 0.5
        _OuterColor("Outer Color",Color) = (1,1,1,1)
        _Scale("Scale",Range(1,8)) = 2
        _Outer("Outer",Range(0,1)) = 0.2
    }
    SubShader
    {
        Pass
        {
            Tags { "RenderType"="Opaque" "Queue"="Transparent"}
            
            Blend SrcAlpha One
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float3 V: TEXCOORD0;
                float3 N : NORMAL;
                float4 pos : SV_POSITION;
            };
            half _Outer;

            v2f vert (appdata v)
            {
                v2f o;
                v.vertex.xyz += v.normal * _Outer;

                o.pos = UnityObjectToClipPos(v.vertex);
                
                o.N = v.normal;
                o.V = ObjSpaceViewDir(v.vertex);

                return o;
            }

            fixed4 _OuterColor;

            half _Scale;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _OuterColor;

                fixed3 N = normalize(i.N);
                fixed3 V = normalize(i.V);

                half rim = pow(saturate(dot(N,V)),_Scale);
                return col * rim;
            }
            ENDCG
        }


        Pass
        {
            Tags { "RenderType"="Opaque" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                float3 L : TEXCOORD1;
                float3 V : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _NormalTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                TANGENT_SPACE_ROTATION;
                o.L = mul((float3x3)rotation,ObjSpaceLightDir(v.vertex));
                o.V = mul((float3x3)rotation,ObjSpaceViewDir(v.vertex));

                return o;
            }
            
            fixed4 _OuterColor;
            fixed _NormalScale;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed3 N = normalize( UnpackNormalWithScale( tex2D(_NormalTex,i.uv) , _NormalScale ));
                fixed3 L = normalize(i.L);
                fixed3 V = normalize(i.V);

                fixed3 diff = _LightColor0.rgb * (dot(L,N) * 0.5 + 0.5);
                fixed rim = pow(saturate(dot(N,V)),0.5);

                fixed4 col = tex2D(_MainTex, i.uv);

                col.rgb *= diff + UNITY_LIGHTMODEL_AMBIENT.rgb;
                col.rgb = lerp(_OuterColor.rgb,col.rgb,rim);
                return col;
            }
            ENDCG
        }
    }

}
