## GrabPass
	在Shader中取屏幕截图

##### 步骤
- 添加一个Pass
` GrabPass{"_GrabTex"} //如果不加名称，则后面用_GrabTexture来获取，但是每个这种脚本都会截屏`
- 添加贴图参数
	```
	sampler2D _GrabTex;
	float4 _GrabTex_ST;
	```
- 取屏幕uv
`ComputeGrabScreenPos(o.vertex)`

- 下面是屏幕uv的采样
	```
	//根据抓屏位置采样Grab贴图,tex2Dproj等同于tex2D(_GrabTex.xy / _GrabTex.w)
	fixed4 grab = tex2Dproj(_GrabTex,i.grab_uv + distor );
	```
- 如果不取屏幕uv，那就和取一般截图的uv没有区别，可以直接用 `TRANSFORM_TEX(v.uv, _GrabTex)` ,采样和普通的贴图一样，直接用`tex2D(_GrabTex,i.uv)`

##### uv旋转
```
float2 swirl(float2 uv,float Angle,float Radius)
{
	//先减去贴图中心点的纹理坐标,这样是方便旋转计算 
	uv -= float2(0.5, 0.5);

	//计算当前坐标与中心点的距离。 
	float dist = length(uv);

	//计算出旋转的百分比 
	float percent = (Radius - dist) / Radius;

	if (percent < 1.0 && percent >= 0.0)
	{
		//通过sin,cos来计算出旋转后的位置。 
		float theta = percent * percent * Angle * 8.0;
		float s = sin(theta);
		float c = cos(theta);
		uv = float2(uv.x*c - uv.y*s, uv.x*s + uv.y*c);
	}
	uv += float2(0.5, 0.5);

	return uv;
}
```

