﻿Shader "Study/GrabPass"
{
    Properties
    {
        _MaskTex ("Mask Tex", 2D) = "white" {}
        _DistorTex("Distor Tex",2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
    }
    SubShader
    {
        GrabPass{"_GrabTex"} //如果不加名称，则后面用_GrabTexture来获取，但是每个这种脚本都会截屏

        Pass
        {
            Tags { "RenderType"="Transparent" "Queue"="Transparent"}
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 mask_uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 grab_uv : TEXCOORD2;
                float2 distor_uv : TEXCOORD3;
            };

            sampler2D _MaskTex;

            sampler2D _GrabTex;
		    float4 _GrabTex_ST;

            sampler2D _DistorTex;
		    float4 _DistorTex_ST;

            float2 swirl(float2 uv)
            {
                //先减去贴图中心点的纹理坐标,这样是方便旋转计算 
                uv -= float2(0.5, 0.5);
 
                //计算当前坐标与中心点的距离。 
                float dist = length(uv);
                float _Radius = 1;
                //计算出旋转的百分比 
                float percent = (_Radius - dist) / _Radius;
                float _Angle = _Time.y;
 
                if (percent < 1.0 && percent >= 0.0)
                {
                    //通过sin,cos来计算出旋转后的位置。 
                    float theta = percent * percent * _Angle * 8.0;
                    float s = sin(theta);
                    float c = cos(theta);
                    uv = float2(uv.x*c - uv.y*s, uv.x*s + uv.y*c);
                }
                uv += float2(0.5, 0.5);
 
                return uv;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.mask_uv = v.uv;

                v.uv = swirl(v.uv);

                o.distor_uv = TRANSFORM_TEX(v.uv, _DistorTex);

                o.grab_uv = ComputeGrabScreenPos(o.vertex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 _Color;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 distor = tex2D(_DistorTex, i.distor_uv ) ;
                fixed4 mask = tex2D(_MaskTex,i.mask_uv);
                fixed alpha = 1 - mask.r* 1.2;

                //根据抓屏位置采样Grab贴图,tex2Dproj等同于tex2D(_GrabTex.xy / _GrabTex.w)
                fixed4 grab = tex2Dproj(_GrabTex,i.grab_uv + distor );
                // sample the texture
                fixed4 col = grab * mask.r ;

                col.a *= alpha*7;

                col *= _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
