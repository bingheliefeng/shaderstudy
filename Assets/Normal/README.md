## 添加法线贴图
	一般用的法线贴图记录的是切空间的向量，所以需要通过切线来计算

- `NormalLight.shader`是转到切空间再进行计算
- `NormalLight1.shader`是先转到切空间，再转成世界空间进行计算，主要是有些功能在世界坐标系更空间计算


#### 实验结果：
1. 最左边是Unity自带的Shader
2. 中间是在切空间计算
3. 右边一个是从切空间转到世界空间后再计算
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Normal/pic1.png)