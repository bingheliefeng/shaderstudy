﻿//在切空间计算
Shader "Study/Normal/Normal Light"
{
    Properties
    {
        _Shininess("Shininess",Range(0,2)) = 0
        _MainTex ("Texture", 2D) = "white" {}
        _BumpTex ("BumpTex",2D) = "White"{}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal:NORMAL;
                float4 tangent:TANGENT;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 L : TEXCOORD2;
                float3 V : TEXCOORD3;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _BumpTex;
            float4 _BumpTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.uv, _BumpTex);

                TANGENT_SPACE_ROTATION;
                o.L = mul((float3x3)rotation,ObjSpaceLightDir(v.vertex));
                o.V = mul((float3x3)rotation,ObjSpaceViewDir(v.vertex));

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed _Shininess;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 L = normalize(i.L);
                float3 V = normalize(i.V);
                float3 N = normalize(UnpackNormal(tex2D(_BumpTex,i.uv.zw)));
                float3 H = normalize(L+V);

                fixed3 diff = _LightColor0.rgb * max(0,dot(N,L)) ;
                fixed3 spec = _LightColor0.rgb * pow( saturate(dot(H,N)), 128 ) * _Shininess;

                // albedo
                fixed4 col = tex2D(_MainTex, i.uv.xy) ;
                col.rgb *= diff + UNITY_LIGHTMODEL_AMBIENT.rgb + spec;
       
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
