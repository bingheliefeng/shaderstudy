﻿//finalcolor-linear fog
Shader "Study/Surface/Surface5"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Specular ("Specular", Range(0,1)) = 0.0
        //需要加这个颜色，才有高光
        _SpecColor ("Spec color", color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "IngnoreProjector"="True"}
        LOD 200

        CGPROGRAM
        #pragma surface surf BlinnPhong noforwardadd noshadow nolightmap finalcolor:myColor vertex:myvert
        #pragma multi_compile_fog

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            half fog;
        };

        half _Glossiness;
        half _Specular;
        fixed4 _Color;
        
        uniform half4 unity_FogStart;
        uniform half4 unity_FogEnd;

        void myvert (inout appdata_full v, out Input data) {
            UNITY_INITIALIZE_OUTPUT(Input,data);
            float pos = length(UnityObjectToViewPos(v.vertex).xyz);
            float diff = unity_FogEnd.x - unity_FogStart.x;
            float invDiff = 1.0f / diff;
            data.fog = saturate ((unity_FogEnd.x - pos) * invDiff);
        }

        void myColor (Input IN, SurfaceOutput o, inout fixed4 color)
        {
            #ifdef UNITY_PASS_FORWARDADD
                UNITY_APPLY_FOG_COLOR(IN.fog, color, float4(0,0,0,0));
            #else
                UNITY_APPLY_FOG_COLOR(IN.fog, color, unity_FogColor);
            #endif
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Gloss = _Glossiness;
            o.Specular = _Specular;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
