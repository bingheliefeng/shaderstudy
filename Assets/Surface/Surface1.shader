﻿//SurfaceOutput结构体在Lighting.cginc中
// struct SurfaceOutput {
//     fixed3 Albedo;
//     fixed3 Normal;
//     fixed3 Emission;
//     half Specular;
//     fixed Gloss;
//     fixed Alpha;
// };
Shader "Study/Surface/Surface1"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Specular ("Specular", Range(0,1)) = 0.0
        //需要加这个颜色，才有高光
        _SpecColor ("Spec color", color) = (0.5,0.5,0.5,0.5)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf BlinnPhong noforwardadd

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 viewDir;
        };

        half _Glossiness;
        half _Specular;
        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;

            o.Gloss = _Glossiness;
            o.Specular = _Specular;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
