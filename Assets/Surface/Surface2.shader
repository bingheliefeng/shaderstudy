﻿//half lambert
Shader "Study/Surface/Surface2"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf MyHalfLambert noforwardadd

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        fixed4 _Color;

        inline fixed4 LightingMyHalfLambert(SurfaceOutput s, fixed3 lightDir , fixed atten)
        {
            fixed diff = dot(lightDir,s.Normal) * 0.5 + 0.5;

            fixed4 c;
            c.rgb = s.Albedo * _LightColor0.rgb * diff;
            c.rgb *= atten;
            c.a = s.Alpha;

            return c;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
