﻿//blinn-phong
Shader "Study/Surface/Surface3"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Specular ("Specular", Range(0,1)) = 0.0
        //需要加这个颜色，才有高光
        _SpecColor ("Spec color", color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf MyBlinnPhong noforwardadd

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Specular;
        fixed4 _Color;

        inline fixed4 LightingMyBlinnPhong(SurfaceOutput s, fixed3 lightDir ,half3 viewDir, fixed atten)
        {
            half3 H = normalize(lightDir + viewDir);

            fixed diff = dot(lightDir,s.Normal) * 0.5 + 0.5;

            float nh = saturate(dot(s.Normal,H));
            float spec = pow(nh, 128.0 * s.Specular) * s.Gloss ;

            fixed4 c;
            c.rgb = s.Albedo * _LightColor0.rgb * diff + _SpecColor.rgb * _LightColor0.rgb * spec;
            c.rgb *= atten;
            c.a = s.Alpha;

            return c;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Gloss = _Glossiness;
            o.Specular = _Specular;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
