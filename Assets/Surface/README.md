## Surface Shader
	Unity内置有的一种超强的Shader
	参考 https://docs.unity3d.com/Manual/SL-SurfaceShaders.html

##### 编译参数
- `#pragma surface surf BlinnPhong/Lambert`，如果用`BlinnPhong`，需要在`Properties`中添加`_SpecColor("Spec color",color)`

- 其他参数
	- 自定义修改函数：
		- vertex:VertexFunction：顶点修改，实现一些顶点动画等。
		- finalcolor:ColorFunction：最终颜色修改，实现雾效等。
		- finalgbuffer:ColorFunction：延迟渲染修改，实现边缘检测等。
		- finalprepass:ColorFunction：prepass base路径修改？
	- 阴影：
		- addshadow：添加一个阴影投射Pass。一般FallBack通用的光照模式中可以找到，对于顶点动画、透明度等需要特殊处理。
		- fullforwardshadows：在前向渲染路径中支持所有光源类型的阴影。默认是只有平行光，添加这个参数就可以让点光或聚光灯有阴影渲染。
		- noshadow：不接受阴影。
	- 透明度混合和测试：
		- alpha、alpha:auto：透明测试。
		- alpha:blend：透明混合。
		- alphatest:VariableName：VariableName用来剔除不满足的片元。
	- 光照：
		- noambient：不应用任何环境光照或光照探针（light probe）。
		- novertexlights：不应用逐顶点光照。
		- noforwardadd：去掉所有前向渲染的额外Pass，即支持逐像素平行光，其他光源用逐顶点或SH计算。
	- 控制代码生成：
		- exclude_path:deferred, exclude_path:forward, exclude_path:prepass ：不需要为特定渲染路径生成代码。
	- lightmap
		- nolightmap , nodynlightmap , nodirlightmap
	- 其他
		- nofog , nometa

	更具体可的参数可参考 https://blog.csdn.net/onafioo/article/details/86585969

##### `SurfaceOutput`结构体
	```
	// Lighting.cginc中定义
	struct SurfaceOutput
	{
		fixed3 Albedo;  // diffuse color
		fixed3 Normal;  // tangent space normal, if written
		fixed3 Emission;
		half Specular;  // specular power in 0..1 range，高光反射指数部分。
		fixed Gloss;    // specular intensity，高光反射强度系数。
		fixed Alpha;    // alpha for transparencies
	};

	// UnityPBSLighing.cginc中定义
	struct SurfaceOutputStandard
	{
		fixed3 Albedo;      // base (diffuse or specular) color
		fixed3 Normal;      // tangent space normal, if written
		half3 Emission;
		half Metallic;      // 0=non-metal, 1=metal
		half Smoothness;    // 0=rough, 1=smooth
		half Occlusion;     // occlusion (default 1)
		fixed Alpha;        // alpha for transparencies
	};
	struct SurfaceOutputStandardSpecular
	{
		fixed3 Albedo;      // diffuse color
		fixed3 Specular;    // specular color
		fixed3 Normal;      // tangent space normal, if written
		half3 Emission;
		half Smoothness;    // 0=rough, 1=smooth
		half Occlusion;     // occlusion (default 1)
		fixed Alpha;        // alpha for transparencies
	};
	```

##### 修改顶点
- 在#param Surface surf... 后面添加`vertex:vert`
- 添加顶点方法，要注意`surface shader`中的顶点着色和`vertex/fragment shader`的写法不一样
	```
	void vert(inout appdata_full v,out Input o)
	{
		UNITY_INITIALIZE_OUTPUT(Input,o);
		v.vertex.xyz += v.normal * _Scale;
	}
	```
##### 修改最终颜色
- 在#param Surface suf... 后面加`finalcolor:mycolor`
- 例如下面修改雾的效果，如果添加了`finalcolor`,那默认就会没有雾效果，需要自己添加代码
	```
	void mycolor (Input IN, SurfaceOutput o, inout fixed4 color)
    {
        fixed3 fogColor = _FogColor.rgb;
        color.rgb = lerp (color.rgb, fogColor, IN.fog);
    }
	```

- `Input` 结构体，需要哪些参数，添加就行了，unity就会把相应的参数算好，传递到surf方法中。
	- `float2 uv_MainTex` 表示获得`_MainTex`这个贴图的`uv`坐标;
	-  其他参数
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Surface/Input.png)