﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CommandBufferTest1 : MonoBehaviour
{
    public Material testMat;
    public CameraEvent cameraEvent = CameraEvent.AfterForwardOpaque;
    // Start is called before the first frame update
    void Start()
    {
        var cf = new CommandBuffer();
        cf.DrawRenderer(GetComponent<MeshRenderer>(), testMat);
        Camera.main.AddCommandBuffer(cameraEvent, cf);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
