## CommandBuffer
	可以在渲染过程中嵌入自定义渲染，单独渲染某个对象，可用于后期处理等效果

#### 重点：
- 导入命名空间 `using UnityEngine.Rendering;`
- 实例化`CommandBuffer`，并嵌入Camera的渲染队列中
	```
	var cf = new CommandBuffer();
	cf.DrawRenderer(GetComponent<MeshRenderer>(), testMat);
	Camera.main.AddCommandBuffer(CameraEvent.AfterForwardOpaque, cf);
	```
- 也可以渲染到一张`RenderTexture`上，例如下面代码：
	```
	commandBuffer = new CommandBuffer();
	commandBuffer.SetRenderTarget(rt);
	commandBuffer.ClearRenderTarget(true, true, Color.clear);
	for (int i = 0; i < targets.Length; i++)
	{
		if (targets[i] == null)
			continue;
		var renders = targets[i].GetComponentsInChildren<Renderer>();
		for (int j = 0; j < renders.Length; j++)
		{
			commandBuffer.DrawRenderer(renders[j], mat_color);
		}
	}
	```
	**具体例子可以参考Outline/Outline3.cs**

#### 实验结果：
图片中的红色部分就是通过CommandBuffer重新渲染的
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/CommandBuffer/pic.png)