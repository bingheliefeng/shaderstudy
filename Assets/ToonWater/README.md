## ToonWater 卡通水
	本例子来自unity官方教程

##### 主要代码
- shader在 `ToonWater/Shaders/ToonWater.shader`
- 需要开启相机的写深度，代码在`ToonWater/Scripts/CameraDepthTextureMode.cs` ,需要把代码挂在相机上

##### 最终效果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/ToonWater/pic.png)