﻿Shader "Unlit/TexColor"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Brightness("亮度",Range(0,2)) = 1
        _Saturation("饱和度",Range(0,2)) = 1
        _Contrast("对比度",Range(0,2)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed _Brightness;
            fixed _Saturation;
            fixed _Contrast;

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                //亮度
                col.rgb *= _Brightness;

                //饱和度
                fixed gray = 0.2125 * col.r + 0.7154 * col.g + 0.0721 * col.b;
                col.rgb = lerp(gray, col.rgb,_Saturation);

                //contrast对比度
			    col.rgb = lerp(fixed3(0.5,0.5,0.5), col.rgb , _Contrast);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
