## 亮度，饱和度，对比度
	
#### 主要算法;
```
fixed4 frag (v2f i) : SV_Target
{
	// sample the texture
	fixed4 col = tex2D(_MainTex, i.uv);

	//亮度
	col.rgb *= _Brightness;

	//饱和度
	fixed gray = 0.2125 * col.r + 0.7154 * col.g + 0.0721 * col.b;
	col.rgb = lerp(gray, col.rgb,_Saturation);

	//contrast对比度
	col.rgb = lerp(fixed3(0.5,0.5,0.5), col.rgb , _Contrast);

	// apply fog
	UNITY_APPLY_FOG(i.fogCoord, col);
	return col;
}
```

#### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Color/pic.png)