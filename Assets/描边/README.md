## 描边
	更复杂的描边算法，可以参考https://github.com/IronWarrior/UnityOutlineShader
	
##### 说明
- 普通描边
	- 需要两个pass
	- 第一个pass显示描边
	- 第二个pass正常显示
	- 参考`Outline` 和 `Outline2` 这两个场景demo
- 通过后期处理来达到描边
	- 主要用于不修改其他模型的shader的情况，能显示描边或发光效果
	- 原理：
		- 创建一个和屏幕一样的`RenderTexture`
		- 创建CommandBuffer，用于将要显示描边的`Renderer`对象画到`RenderTexture`上
		- 在用CommandBufferDrawRenderer时，直接画一个大于模型的纯色，参考`Outline_Color.shader`
		- `void OnRenderImage(RenderTexture source, RenderTexture destination)` 最后再通过后期，使用`Outline_Post.shader`把场景真实图和描边图进行叠加显示
		- 具体查看 `Outline3` 这个场景Demo
	- 参考 https://www.cnblogs.com/alps/p/7606028.html

##### 顶点中的算法
```
	//将法线转到视空间
	float3 vNormal = COMPUTE_VIEW_NORMAL;//normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal));
	//再转到投影空间
	float2 projPos = normalize(TransformViewToProjection(vNormal.xy));
	//边缘发光
	o.vertex.xy += projPos * _Outline;
```
- 此方式有一定的缺点，描边有一些断开
- 解决办法是对模型的切线进行处理，然后通过切线的方向来计算
```
	private void WirteAverageNormalToTangent(Mesh mesh)
	{
		var vertics = mesh.vertices;
		var normals = mesh.normals;

		var averageNormalHash = new System.Collections.Generic.Dictionary<Vector3, Vector3>();
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			if (!averageNormalHash.ContainsKey(vertics[j]))
			{
				averageNormalHash.Add(vertics[j], normals[j]);
			}
			else
			{
				averageNormalHash[vertics[j]] =
					(averageNormalHash[vertics[j]] + normals[j]).normalized;
			}
		}

		var averageNormals = new Vector3[mesh.vertexCount];
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			averageNormals[j] = averageNormalHash[vertics[j]];
		}

		var tangents = new Vector4[mesh.vertexCount];
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			tangents[j] = new Vector4(averageNormals[j].x, averageNormals[j].y, averageNormals[j].z, 0);
		}
		mesh.tangents = tangents;
	}
```
- 使用python来修改fbx
	- FBXSDK下载地址：https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-2020-1-1
	- `fbx/fbx_tangent.py`是我写的一个计算切线的脚本，可以把平滑写到fbx的切线数据中
	- 切换来描边的效果请参考`Outline2`这个场景

##### 实验结果：
	第一张是常用的在视空间中直接拉出描边
	第二张是通过重新计算切线来达到描边
	第三张是通过后期处理方式来描边
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E6%8F%8F%E8%BE%B9/pic1.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E6%8F%8F%E8%BE%B9/pic2.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/%E6%8F%8F%E8%BE%B9/pic3.png)