﻿using UnityEngine;

public class NormalToTangent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var renders = GetComponentsInChildren<Renderer>();
        foreach(var r in renders)
        {
            Mesh mesh = null;
            if(r is SkinnedMeshRenderer)
            {
                mesh = (r as SkinnedMeshRenderer).sharedMesh;
            }
            else
            {
                MeshFilter mf = r.GetComponent<MeshFilter>();
                if(mf)
                {
                    mesh = mf.sharedMesh;
                }
            }
            if(mesh)
            {
                WirteAverageNormalToTangent(mesh);
            }
        }
    }

    private void WirteAverageNormalToTangent(Mesh mesh)
	{
		var vertics = mesh.vertices;
		var normals = mesh.normals;

		var averageNormalHash = new System.Collections.Generic.Dictionary<Vector3, Vector3>();
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			if (!averageNormalHash.ContainsKey(vertics[j]))
			{
				averageNormalHash.Add(vertics[j], normals[j]);
			}
			else
			{
				averageNormalHash[vertics[j]] =
					(averageNormalHash[vertics[j]] + normals[j]).normalized;
			}
		}

		var averageNormals = new Vector3[mesh.vertexCount];
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			averageNormals[j] = averageNormalHash[vertics[j]];
		}

		var tangents = new Vector4[mesh.vertexCount];
		for (var j = 0; j < mesh.vertexCount; j++)
		{
			tangents[j] = new Vector4(averageNormals[j].x, averageNormals[j].y, averageNormals[j].z, 0);
		}
		mesh.tangents = tangents;
	}
}
