﻿using UnityEngine;
using UnityEngine.Rendering;

public class Outline3 : MonoBehaviour
{
    public Material mat_color, mat_post;
    public GameObject[] targets;

    private CommandBuffer commandBuffer;
    private RenderTexture lineRT;

    void Start()
    {
        lineRT = RenderTexture.GetTemporary(Screen.width , Screen.height , 0);

        RefreshCommandBuffer();
    }

    /// <summary>
    /// 如果数量有变化时，需要重新调用此方法
    /// </summary>
    void RefreshCommandBuffer()
    {
        commandBuffer = new CommandBuffer();
        commandBuffer.SetRenderTarget(lineRT);
        commandBuffer.ClearRenderTarget(true, true, Color.clear);
        for (int i = 0; i < targets.Length; i++)
        {
            if (targets[i] == null)
                continue;
            var renders = targets[i].GetComponentsInChildren<Renderer>();
            for (int j = 0; j < renders.Length; j++)
            {
                commandBuffer.DrawRenderer(renders[j], mat_color);
            }
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (mat_color != null && mat_post != null && lineRT != null && targets != null)
        {
            Graphics.ExecuteCommandBuffer(commandBuffer);

            mat_post.SetTexture("_SceneTex", source);
            Graphics.Blit(lineRT, destination, mat_post);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }

    private void OnDestroy()
    {
        if (commandBuffer != null) commandBuffer.Clear();
        if (lineRT) RenderTexture.ReleaseTemporary(lineRT);
        lineRT = null;
    }
}
