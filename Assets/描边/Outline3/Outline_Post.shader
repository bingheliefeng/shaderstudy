﻿Shader "Study/Outline/Outline_Post"
{
    Properties
    {
        [HideInInspector]
        _MainTex("Outline Tex",2D) = "White"{}
        [HideInInspector]
        _SceneTex("Scene Tex",2D) = "White"{}
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(2)
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            sampler2D _MainTex;
            sampler2D _SceneTex;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_SceneTex, i.uv);
                fixed4 outline = tex2D(_MainTex,i.uv);
                fixed a = outline.a - col.a;
                col.rgb = lerp(col.rgb,outline.rgb, a>0 ? 1.0 : 0.0);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
