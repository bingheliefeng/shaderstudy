﻿Shader "Study/Outline/Outline_Color"
{
    Properties
    {
        _Color("Color",Color) = (1,1,0,1)
        _Outline("Outline",Range(0.0,1.0)) = .2
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            fixed _Outline;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float3 nView = normalize( mul((float3x3)UNITY_MATRIX_IT_MV,v.normal));
                float2 offset = normalize( mul((float2x2)UNITY_MATRIX_P,nView.xy));
                o.vertex.xy += offset * _Outline ;
                return o;
            }

            fixed4 _Color;

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
