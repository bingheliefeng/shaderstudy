﻿#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import FbxCommon
from fbx import *

class FBX_tangent(object):
    def __init__(self,filename):
        self.filename = filename
        self.scene = None
        self.sdk_manager = None
        self.sdk_manager, self.scene = FbxCommon.InitializeSdkObjects()
        FbxCommon.LoadScene(self.sdk_manager,self.scene,filename)
        self.root_node = self.scene.GetRootNode()

    def calTangent(self):
        self.mesh_nodes = self.get_mesh_nodes()
        for pNode in self.mesh_nodes:
            lMesh = pNode.GetNodeAttribute ()
            self.__calMesh(lMesh)

        lResult = FbxCommon.SaveScene(self.sdk_manager, self.scene, self.filename)
        if lResult == False:
            print("\nAn error occurred while saving the scene...\n")
        else:
            print "\nfbx process success\n"

            

    def __calMesh(self,pMesh):
        if pMesh == None :
            return

        vertexCount = pMesh.GetPolygonVertexCount() #顶点数量
        vertices = pMesh.GetPolygonVertices() #顶点索引
        normals = pMesh.GetLayer(0).GetNormals().GetDirectArray() #GetDirectArray()

        averageNormalHash = {} #dictionary
        for j in range(vertexCount):
            if not averageNormalHash.has_key(vertices[j]) :
                averageNormalHash[vertices[j]] = normals.GetAt(j)
            else:
                tmp = normals.GetAt(j)
                tmp.Normalize()
                averageNormalHash[vertices[j]] += tmp
        averageNormals = []
        for j in range(vertexCount):
            averageNormals.append( averageNormalHash[vertices[j]])

        # 创建tangent
        tangents = FbxLayerElementTangent.Create(pMesh,"") 
        tangents.SetMappingMode(FbxLayerElement.eByPolygonVertex)
        tangents.SetReferenceMode(FbxLayerElement.eDirect)

        binNormals = FbxLayerElementBinormal.Create(pMesh,"")
        binNormals.SetMappingMode(FbxLayerElement.eByPolygonVertex)
        binNormals.SetReferenceMode(FbxLayerElement.eDirect)

        for j in range(vertexCount):
            fbxv = FbxVector4(averageNormals[j][0],averageNormals[j][1],averageNormals[j][2],0)
            fbxv.Normalize()
            tangents.GetDirectArray().Add(fbxv)
            binNormals.GetDirectArray().Add(normals[j])
           
        pMesh.GetLayer(0).SetTangents(tangents)
        pMesh.GetLayer(0).SetBinormals(binNormals)
        

    #递归
    def __get_scene_nodes_recursive(self,node):
        atrr = node.GetNodeAttribute()
        if atrr != None :
            lAttributeType = atrr.GetAttributeType()
            if lAttributeType == FbxNodeAttribute.eMesh:
                self.mesh_nodes.append(node)
        
        for i in range(node.GetChildCount()):
            self.__get_scene_nodes_recursive(node.GetChild(i))

    def get_mesh_nodes(self):
        self.mesh_nodes=[]
      
        for i in range(self.root_node.GetChildCount()):
            self.__get_scene_nodes_recursive(self.root_node.GetChild(i))
        return self.mesh_nodes

    def close(self):
        self.sdk_manager.Destroy()

#####################################
# python fbx_tangent.py baoan.FBX
len = len(sys.argv)
if len>1 :
    fn = str(sys.argv[1])
    fbxTangent = FBX_tangent(fn)
    fbxTangent.calTangent()
    fbxTangent.close()
else:
    print "需要一个文件参数"
