﻿Shader "Study/Outline4"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _OutlineColor("Color",Color) = (1,1,0,1)
        _Factor("Factor",Range(0,1)) = 0.5
        _Outline("Outline",Range(0,2)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Cull Front
            ZWrite On

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal:NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            fixed4 _OutlineColor;
            fixed _Outline;
            fixed _Factor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                float3 dir = normalize(v.vertex.xyz);
                dir = lerp(dir,v.normal,_Factor);
                dir = mul((float3x3)UNITY_MATRIX_IT_MV, dir);
                //转到投影空间
                float2 projPos = normalize( TransformViewToProjection(dir.xy) );
                
                //描边
                o.vertex.xy += projPos * _Outline * 0.02 * clamp(0,10,o.vertex.w);
                
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = _OutlineColor;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal:NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 worldNormal:NORMAL;
                float3 worldPos:TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 N = normalize( i.worldNormal );
                float3 L = UnityWorldSpaceLightDir( i.worldPos );
                float3 V = UnityWorldSpaceViewDir( i.worldPos );
                float3 H = normalize(L+V);
                fixed3 diff = _LightColor0.rgb * (dot(L,N)*0.5+0.5);
                fixed3 sepc = _LightColor0.rgb * pow( saturate(dot(H,N)), 2 * 128.0 ) * 0.5;
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb *= UNITY_LIGHTMODEL_AMBIENT.rgb + diff ;//+ sepc;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
