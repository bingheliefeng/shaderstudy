## PBS
	本章节主要是研究Unity自带的PBS

##### PBS 原理
- PBS 是指 Physiclly based shading, 基于物理的着色
- BRDF 是 PBS 的算法实现
- 需要遵循能量守恒原则，即反射光 <= 入射光，所以需要根据材料的粗糙度，光滑度等来进行计算

##### Unity内置方法
- 在 `UnityStandardBRDF.cgic` 中，有BRDF的3个实现（BRD(F1-3)_Unity_PBS），其实第1个质量最好
- 我们在自定义 BRDF 时，可以只引用 `UnityPBSLighting.cginc` 就可以了，里面就包含了该有的光照库
- `albedo = DiffuseAndSpecularFromMetallic(albedo,_Metallic,specColor,oneMinusReflectivity);` 这个方法用来计算高光和反射值，后面两位都是out参数
- `fixed4 col = BRDF1_Unity_PBS(albedo,specColor,  oneMinusReflectivity, _Smoothness, N, V, DirectLight,InDirectLight);` 最后通过这个方法来得到BRDF结果，当然也可以直接用`UNITY_BRDF_PBS`宏来执行

##### 重点：
1. 和之前的光照计算一样，需要得到 法线向量，世界坐标系的光照方向`(UnityWorldSpaceLightDir(i.worldPos))`，世界坐标系的视方向`(UnityWorldSpaceViewDir(i.worldPos))`
	- 里面的 `i.worldPos` 是在顶点计算中用 `mul(unity_ObjectToWorld,i.vertex)` 将模型坐标系转到世界坐标系
2. 法向量也是和之前一样，需要在顶点计算中用 `UnityObjectToWorldNormal(i.normal)` 来转换
3. `UnityLight` 和 `UnityIndirect` 是直接光和间接光
	- 直接光照一般是直线光的参数
	- 间接光我这里用的环境光
4. 关键代码
```
fixed4 frag (v2f i) : SV_Target
{
	float3 N = normalize(i.worldNormal);
	float3 L = normalize(UnityWorldSpaceLightDir(i.worldPos));
	float3 V = normalize(UnityWorldSpaceViewDir(i.worldPos));

	half3 albedo = tex2D(_MainTex, i.uv);

	half3 specColor ; //out
	half oneMinusReflectivity; //out
	albedo = DiffuseAndSpecularFromMetallic(albedo,_Metallic,specColor,oneMinusReflectivity);

	UnityLight DirectLight;
	DirectLight.dir = L ;
	DirectLight.color = _LightColor0.rgb;

	UnityIndirect InDirectLight;
	InDirectLight.diffuse = UNITY_LIGHTMODEL_AMBIENT.rgb;
	InDirectLight.specular = .5;

	//BRDF(1-3)_Unity_PBS 其中1的效果最好
	fixed4 col = BRDF1_Unity_PBS(albedo,specColor,
								oneMinusReflectivity,
								_Smoothness,
								N, V,
								DirectLight,InDirectLight);
	// apply fog
	UNITY_APPLY_FOG(i.fogCoord, col);
	return col;
}
```
##### 实验结果：
	下图中，左边模型是Unity自边的Surface Shader，右边模型有我自己写的 PBS

![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/PBS/pic.png)