﻿Shader "Unlit/UNITY_PBS"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Metallic("Metallic",Range(0.0,1.0)) = 0.0
        _Smoothness("Smoothness",Range(0.0,1.0)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(2)
                float4 vertex : SV_POSITION;
                float3 worldNormal : NORMAL;
                float3 worldPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed _Metallic;
            fixed _Smoothness;

            fixed4 frag (v2f i) : SV_Target
            {
                float3 N = normalize(i.worldNormal);
                float3 L = normalize(UnityWorldSpaceLightDir(i.worldPos));
                float3 V = normalize(UnityWorldSpaceViewDir(i.worldPos));

                half3 albedo = tex2D(_MainTex, i.uv);

                half3 specColor ; //out
                half oneMinusReflectivity; //out
                albedo = DiffuseAndSpecularFromMetallic(albedo,_Metallic,specColor,oneMinusReflectivity);

                UnityLight DirectLight;
                DirectLight.dir = L ;
                DirectLight.color = _LightColor0.rgb;
            
                UnityIndirect InDirectLight;
                InDirectLight.diffuse = UNITY_LIGHTMODEL_AMBIENT.rgb;
                InDirectLight.specular = .5;

                //BRDF(1-3)_Unity_PBS 其中1的效果最好
                fixed4 col = BRDF1_Unity_PBS(albedo,specColor,
                                            oneMinusReflectivity,
                                            _Smoothness,
                                            N, V,
                                            DirectLight,InDirectLight);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
