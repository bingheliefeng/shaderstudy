﻿Shader "Study/multi_compile_test"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [KeywordEnum(Off,On)] Diff("Diffuse",Float) = 1
        [MaterialToggle] Spec("Spec",Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            #pragma multi_compile _ DIFF_OFF DIFF_ON
            #pragma multi_compile _ SPEC_ON

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 N : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 N : NORMAL;
                float3 L : TEXCOORD1;
                float3 V : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.N = v.N;
                o.L = ObjSpaceLightDir(v.vertex);
                o.V = ObjSpaceViewDir(v.vertex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);


                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);

                #if DIFF_ON
                col.rgb *= _LightColor0.rgb * saturate(dot(L,N)) + UNITY_LIGHTMODEL_AMBIENT.rgb;
                #endif

                #if SPEC_ON
                fixed3 V = normalize(i.V);
                fixed3 H = normalize(L+V);
                col.rgb += _LightColor0.rgb * pow(saturate(dot(N,H)),128 * 0.1) * 0.8;
                #endif

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
