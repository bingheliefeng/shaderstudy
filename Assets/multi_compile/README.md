## Shader变种
- multi_complie
	全局数量有限制，会把所有的变种shader都打包进去
- shader_feature
	只打包用到的变种，如果有没有用到的，后期再用`Material.EnableShaderKeyword` 或 `Shader.EnableShaderKeyword` 就可能没效果

##### Editor调试
	有一些shader关键词可以便于变种调试
- 例如在shader的Properties中添加
```
[KeywordEnum(Off,On)] Diff("Diffuse",Float) = 1
[MaterialToggle] Spec("Spec",Float) = 1
```

- 后面编译参数
```
#pragma multi_compile _ DIFF_OFF DIFF_ON
#pragma multi_compile _ SPEC_ON
```
或
```
#pragma shader_feature _  DIFF_OFF DIFF_ON
#pragma shader_feature _ SPEC_ON
```
---
## 通过Lod来启用和禁用subshader
- 首先在shader中写多个Subshader，将Lod大的Subshader放前面
- 在c#代码里用`Shader.globalMaximumLOD = lod;`来设置LOD值，大于shader中的lod时，那个subshader就会启用
##### 结果
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/multi_compile/pic.png)