﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class multi_pass_enable : MonoBehaviour
{
    [Range(10,500)]
    public int lod = 110;
    // Start is called before the first frame update
    void Start()
    {
        Shader.globalMaximumLOD = lod;
    }

    // Update is called once per frame
    void Update()
    {
        Shader.globalMaximumLOD = lod;
    }
}
