﻿Shader "Study/CustomFoam2"
{
    Properties
	{
		_MainTex("Main Texture", 2D) = "White"{}

		[Space]
		_FoamStartY("Foam StartY",Range(-0.1,0.1)) = -0.08
		_FoamLength("Foam Width",Range(0.002,0.05)) = 0.03
		_FoamAlpha("Foam Alpha",Range(0,1)) = 1
	}

	SubShader
	{
        Tags { "Queue" = "Geometry" "RenderType"="Opaque" "IgnoreProjector"="True" "LightMode"="ForwardBase"}
		LOD 100
		Pass
		{
			CGPROGRAM
            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			#pragma vertex vert
			#pragma fragment frag

			struct a2v
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal:NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 L : TEXCOORD1;
				float posY : TEXCOORD2;
			};

            sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert(a2v i)
			{
				v2f o;
                o.uv = TRANSFORM_TEX(i.uv,_MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);

				o.posY = mul(unity_ObjectToWorld,i.vertex).y;
				float3 L = normalize(ObjSpaceLightDir(i.vertex));
				o.L = _LightColor0.rgb * (dot(i.normal,L)*0.5+0.5) + UNITY_LIGHTMODEL_AMBIENT.rgb;
				return o;
			}

			half _FoamStartY;
            half _FoamLength;
			fixed _FoamAlpha;

			float4 frag(v2f i) : SV_Target
			{
                fixed4 mainCol = tex2D(_MainTex,i.uv);
                fixed4 col = mainCol;

				//光照
				col.rgb *= i.L;

				//转到0-1
				float depth =smoothstep( ( _FoamStartY + _FoamLength - i.posY)/_FoamLength , 0 , 1);
				float foamLine = 1 - depth;
				col += lerp(foamLine , 1 , 1-depth) * _FoamAlpha;

                return col;
			}

			ENDCG
		}
	}
}
