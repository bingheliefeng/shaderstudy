﻿Shader "Study/CustomFoam"
{
    Properties
	{
		_MainTex("Main Texture", 2D) = "White"{}
        _DepthStart("Depth Start", Float) = 0
        _DepthEnd("Depth End", Float) = 0.5

		[Space]
		_DepthColor("Depth Color", color) = (1,1,1,1)

		[Space]
		_FoamTex("Foam Texture",2D) = "White"{}
		_FoamEdgeDepth("Foam Depth",Range(0.01,10)) = 1
		_FoamSpeed("Foam Speed",Range(0,1)) = 0.1
		_FoamDistortTex("Foam Distort Tex",2D) = "White"{}
		_FoamDistort("Foam Distort Range",Range(0,1))=1
	}

	SubShader
	{
        Tags { "Queue" = "Geometry" "RenderType"="Opaque" "IgnoreProjector"="True" "LightMode"="ForwardBase"}
		LOD 100
		Pass
		{
			CGPROGRAM
            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			#pragma vertex vert
			#pragma fragment frag

			struct a2v
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal:NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 foam_uv : TEXCOORD1;
				float3 worldNormal : NORMAL;
				float3 worldPos : TEXCOORD2;
			};

			sampler2D _FoamTex;
			float4 _FoamTex_ST;
			fixed _FoamSpeed;

			sampler2D _FoamDistortTex;
			float4 _FoamDistortTex_ST;

            sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert(a2v i)
			{
				v2f o;
                o.uv = TRANSFORM_TEX(i.uv,_MainTex);
				o.pos = UnityObjectToClipPos(i.vertex);

				o.worldPos = mul(unity_ObjectToWorld,i.vertex);
				o.worldNormal = UnityObjectToWorldNormal(i.normal);

				o.foam_uv.xy = TRANSFORM_TEX(i.uv,_FoamTex);
				o.foam_uv.zw = TRANSFORM_TEX(i.uv,_FoamDistortTex) - float2(_Time.y * _FoamSpeed * 0.1,0) ;
				return o;
			}


			fixed4 _DepthColor;

			half _FoamEdgeDepth;
			fixed _FoamDistort;

			half _DepthStart;
            half _DepthEnd;

			float4 frag(v2f i) : SV_Target
			{
                fixed4 mainCol = tex2D(_MainTex,i.uv);
                fixed4 col = mainCol;

				//光照
				float3 N = normalize(i.worldNormal);
				float3 L = normalize(UnityWorldSpaceLightDir(i.worldPos));
				fixed3 diff = _LightColor0.rgb * (dot(N,L)*0.5+0.5) + UNITY_LIGHTMODEL_AMBIENT.rgb;

				col.rgb *= diff;


				//转到0-1
				float depth = saturate( (_DepthEnd-i.worldPos.y)/(_DepthEnd-_DepthStart)); 
				//深度颜色叠加
				col = lerp( _DepthColor ,mainCol, depth);

				// Foam
				fixed4 foamDistort = tex2D(_FoamDistortTex,i.foam_uv.zw );
				fixed4 foam = tex2D(_FoamTex,i.foam_uv.xy + foamDistort * _FoamDistort);
				foam *= 1 - depth;

				float foamLine = 1 - saturate(_FoamEdgeDepth * depth );
				col += lerp(foamLine * foam, mainCol ,pow(1-depth,8)) ;

                return col;
			}

			ENDCG
		}
	}
}
