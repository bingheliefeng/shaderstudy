﻿Shader "Study/Water/Foam"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)

		[Space]
		_DepthBlend("Depth",Range(0,5)) = 1
		_DepthColor("Depth Color", color) = (1,1,1,1)

		[Space]
		_FoamTex("Foam Texture",2D) = "White"{}
		_FoamEdgeDepth("Foam Depth",Range(0,10)) = 1
		_FoamSpeed("Foam Speed",Range(0,1)) = 0.1
		_FoamDistortTex("Foam Distort Tex",2D) = "White"{}
		_FoamDistort("Foam Distort Range",Range(0,1))=1
	}

	SubShader
	{
        Tags { "Queue" = "Transparent" "RenderType"="Transparent" "IgnoreProjector"="True"}
		
		ZWrite Off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			#pragma vertex vert
			#pragma fragment frag

			struct a2v
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal:NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 foam_uv : TEXCOORD0;
				float4 screenPos : TEXCOORD1;
				float3 worldNormal : NORMAL;
				float3 worldPos : TEXCOORD2;
			};

			sampler2D _FoamTex;
			float4 _FoamTex_ST;

			sampler2D _FoamDistortTex;
			float4 _FoamDistortTex_ST;

			v2f vert(a2v i)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(i.vertex);

				o.worldPos = mul(unity_ObjectToWorld,i.vertex);
				o.worldNormal = UnityObjectToWorldNormal(i.normal);

				// 计算屏幕空间深度
				o.screenPos = ComputeScreenPos(o.pos);

				o.foam_uv.xy = TRANSFORM_TEX(i.uv,_FoamTex);
				o.foam_uv.zw = TRANSFORM_TEX(i.uv,_FoamDistortTex);
				return o;
			}

			fixed4 _Color;

			fixed4 _DepthColor;
			half _DepthBlend;

			half _FoamEdgeDepth;
			fixed _FoamSpeed;
			fixed _FoamDistort;

			sampler2D _CameraDepthTexture;

			float4 frag(v2f i) : COLOR
			{
				//采样深度图
				float4 depthSample = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, i.screenPos);
				//转到0-1
				float depth = LinearEyeDepth(depthSample) - i.screenPos.w;
				//深度颜色叠加
				fixed4 col = lerp( _DepthColor ,_Color, 1 - saturate(_DepthBlend * depth) );

				// Foam
				fixed4 foamDistort = tex2D(_FoamDistortTex,i.foam_uv.zw- _Time * _FoamSpeed * 0.01);
				fixed4 foam = tex2D(_FoamTex,i.foam_uv.xy + foamDistort * _FoamDistort);
				foam *= 1 - depth;

				float foamLine = 1 - saturate(_FoamEdgeDepth * depth );
				col += lerp(foamLine * foam,fixed4(1,1,1,1),pow(saturate(1-depth),8)) ;


				//光照
				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
				float3 N = normalize(i.worldNormal);
				float3 V = normalize(UnityWorldSpaceViewDir(i.worldPos));
				float3 L = normalize(UnityWorldSpaceLightDir(i.worldPos));
				fixed3 diff = _LightColor0.rgb * (dot(N,L)*0.5+0.5);
				half fresnel = 2.8 * pow(1 - saturate(dot(N,V)), 2 );

				col.rgb *= (ambient + diff );
				col.rgb = lerp(col.rgb,_LightColor0.rgb,fresnel);

                return col;
			}

			ENDCG
		}
	}
}