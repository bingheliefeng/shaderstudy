## water
1. 为ShadowGun中的水，适合静态小水池，偏卡通
2. 为有折射，有光照，fresnel效果，偏卡通
3. 边缘有Foam，通过深度图来生成
    - 如果是向前渲染，需要把 `Camera.main.depthTextureMode = DepthTextureMode.Depth;`
    - 这样可以在shader中通过 `sampler2D _CameraDepthTexture;` 来获深度图
        ```
        //采样深度图
        float4 depthSample = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, i.screenPos);
        //LinearEyeDepth: Z buffer to linear depth
        float depth = LinearEyeDepth(depthSample) - i.screenPos.w;
        //深度颜色叠加
        fixed4 col = lerp( _DepthColor ,_Color, 1 - saturate(_DepthBlend * depth) );
        ```
    - 参考 https://www.jianshu.com/p/80a932d1f11e
4. 一般手机游戏常用水,偏写实，通过顶点颜色来区分深浅 https://github.com/AsehesL/WaterByBakeDepth
5. 还有一种假的水花，通过计算在世界中的Y坐标来显示水花，下面的图片中的第5张图就是假的水花
##### 图片
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Water/pic1.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Water/pic2.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Water/pic3.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Water/pic4.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Water/pic5.png)