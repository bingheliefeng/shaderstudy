﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraDepth : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
    }
    
}
