﻿Shader "Study/Water/Water Refract" {
	Properties {
		_Color("Color",Color) = (1,1,1,1)
		_WaveScale ("Wave scale", Range (0.02,0.15)) = 0.063
		_RefrDistort ("Refraction distort", Range (0,1.5)) = 0.40
		_RefrColor ("Refraction color", COLOR)  = ( .34, .85, .92, 1)
		_BumpMap ("Normalmap ", 2D) = "bump" {} //用来产生波纹的法线贴图.
		WaveSpeed ("Wave speed (map1 x,y; map2 x,y)", Vector) = (19,9,-16,-7)
		[HideInInspector]_RefractionTex ("Internal Refraction", 2D) = "" {}  //折射用的renderTexture
	}

	Subshader { 
		Tags { "WaterMode"="Refractive" "RenderType"="Opaque" "IgnoreProjector"="True" "LightMode"="ForwardBase"}
		
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"
				#include "Lighting.cginc"

				fixed4 _WaveScale4;
				fixed _RefrDistort;
				half4 _WaveOffset;

				struct appdata {
					float4 vertex : POSITION;
					float3 normal : NORMAL;
				};

				struct v2f {
					float4 pos : SV_POSITION;
					float4 ref : TEXCOORD0;
					float4 bumpuv : TEXCOORD1;
					float3 worldPos: TEXCOORD2;
					float3 worldNormal: TEXCOORD3;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos (v.vertex);
					float4 temp;
					float4 wpos = mul (unity_ObjectToWorld, v.vertex);
					temp.xyzw = wpos.xzxz * _WaveScale4 + _WaveOffset;
					o.bumpuv = temp;

					o.worldPos = wpos;
					o.worldNormal = UnityObjectToWorldNormal(v.normal);
					
					o.ref = ComputeScreenPos(o.pos);
					return o;
				}

				sampler2D _ReflectionTex;
				sampler2D _BumpMap;
				sampler2D _RefractionTex;
				fixed4 _RefrColor;
				fixed4 _Color;

				half4 frag( v2f i ) : COLOR
				{
					// combine two scrolling bumpmaps into one
					fixed3 bump1 = UnpackNormal(tex2D( _BumpMap, i.bumpuv.xy )).rgb;
					fixed3 bump2 = UnpackNormal(tex2D( _BumpMap, i.bumpuv.zw )).rgb;
					fixed3 bump = (bump1 + bump2) * 0.5;
					
					float4 uv2 = i.ref; uv2.xy -= bump * _RefrDistort;
					fixed4 refr = tex2Dproj( _RefractionTex, UNITY_PROJ_COORD(uv2) ) * _RefrColor;
					fixed4 col = refr+_Color;

					//light
					fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
					float3 N = normalize( i.worldNormal * fixed3(bump.x,bump.y+0.6,bump.z) );
					float3 V = normalize(UnityWorldSpaceViewDir(i.worldPos));
					float3 L = normalize(UnityWorldSpaceLightDir(i.worldPos));
					
					fixed LDotN = max(0,dot(L,N));
					fixed VDotN = max(0,dot(V,N));
					fixed3 diff = _LightColor0.rgb * LDotN;
					fixed fresnel = 0.1 + 1.5 * pow(1 - VDotN,1.5);

					col.rgb *= ambient + diff;
					col.rgb = lerp(col.rgb,_LightColor0.rgb,fresnel);
					return col;
				}
			ENDCG

		}
	}
}
