﻿Shader "Study/Water/Water Foam"
{
	Properties
	{
		_Color("Color",Color) = (1,1,1,1)
		_FoamInWidth("Foma In Width",Range(0.01,1)) = 0.2 

		[Space]
		_DistortTex("Distort Texture", 2D) = "black" {}
		_DistortScrollX ("Distort Scroll X" ,Range(-2,2)) = 0.01
		_DistortScrollY ("Distort Scroll Y" ,Range(-2,2)) = 0.2
		_DistortScale("Distort Scale",Range(0,1)) = 0.2

		[Space]
		_MaskTex("Mask Texture",2D) = "white"{}

		[Space]
		_AlphaClip("Alpha Clip",Range(0,1)) = 0.2
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"  }

		Cull Back
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 uv : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			sampler2D _DistortTex;
			float4 _DistortTex_ST;
			
			float _DistortScrollX;
			float _DistortScrollY;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv.xy = v.uv;
				o.uv.zw = TRANSFORM_TEX(v.uv, _DistortTex) - float2(_Time.y*_DistortScrollX,_Time.y*_DistortScrollY);

				return o;
			}

			fixed _DistortScale;
			fixed _FoamInWidth;
			fixed4 _Color;
			fixed _AlphaClip;

			sampler2D _MaskTex;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 distori = tex2D(_DistortTex, i.uv.zw) *  _DistortScale;

				fixed4 col = _Color;
				col.a *= tex2D(_MaskTex,i.uv.xy).r;
				
				float len = _FoamInWidth - i.uv.y;

				clip( distori.r + len - _AlphaClip );

				return col;
			}
			ENDCG
		}
	}
}
