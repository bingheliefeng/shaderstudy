﻿//顶点弯曲
Shader "Study/Vertex/Vertex Bend"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _QOffset("Offset",Vector)=(0,0,0,0)
        _Dist("Start Bend Distance",Float) = 5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;

                float3 L : TEXCOORD2;
                float3 N : NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _Dist;
            float4 _QOffset;

            v2f vert (appdata v)
            {
                v2f o;
                float4 vPos = mul (UNITY_MATRIX_MV, v.vertex);
                float zOff = vPos.z/_Dist;
                vPos += _QOffset*zOff*zOff;
                o.vertex = mul (UNITY_MATRIX_P, vPos);

                o.L = ObjSpaceLightDir(v.vertex);
                o.N = v.normal;

                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                fixed3 N = normalize(i.N);
                fixed3 L = normalize(i.L);
                fixed4 diff = _LightColor0 * (dot(N,L) * 0.5 + 0.5 );

                col *= diff;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
