﻿Shader "CrazyShader/FlagWind"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		// 风的方向
		_Wind ("Wind Params", Vector) = (1, 1, 1, 1)

		// 边缘移动的距离 也就是移动幅度
		_WindEdgeFlutter("Wind Edge flutter factor", float) = 0.5

		_WindEdgeFlutterFreqScale("Wind edge flutter freq scale", float) = 0.5
	}
	SubShader
	{
		Cull off

		CGINCLUDE
		#include "UnityCG.cginc"
		#include "TerrainEngine.cginc"
		#include "Lighting.cginc"

		sampler2D _MainTex;
		float4 _MainTex_ST;
		
		float _WindEdgeFlutter;
		float _WindEdgeFlutterFreqScale;

		struct v2f
		{
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			fixed4 color : COLOR; //顶点颜色
			float3 N : NORMAL ;
			float3 L : TEXCOORD1;
		};

		// 修改顶点的函数
		inline float4 AnimateVertex2(float4 pos, float3 normal, float4 animParams, float4 wind, float2 time)
		{
			// animParams stored in color
			// animParams.x = branch phase
			// animParams.y = edge flutter factor
			// animParams.z = primary factor
			// animParams.w = secondary factor

			float fDetailAmp = 0.1f;
			float fBranchAmp = 0.3f;
			
			// Phases (object, vertex, branch)
			float fObjPhase = dot(unity_ObjectToWorld[3].xyz, 1);
			float fBranchPhase = fObjPhase + animParams.x;
			
			float fVtxPhase = dot(pos.xyz, animParams.y + fBranchPhase);
			
			// x is used for edges; y is used for branches
			float2 vWavesIn = time  + float2(fVtxPhase, fBranchPhase );
			
			// 1.975, 0.793, 0.375, 0.193 are good frequencies
			float4 vWaves = (frac( vWavesIn.xxyy * float4(1.975, 0.793, 0.375, 0.193) ) * 2.0 - 1.0);
			
			vWaves = SmoothTriangleWave( vWaves );
			float2 vWavesSum = vWaves.xz + vWaves.yw;

			// Edge (xz) and branch bending (y)
			float3 bend = animParams.y * fDetailAmp * normal.xyz;
			bend.y = animParams.w * fBranchAmp;
			pos.xyz += ((vWavesSum.xyx * bend) + (wind.xyz * vWavesSum.y * animParams.w)) * wind.w; 

			// Primary bending
			// Displace position
			pos.xyz += animParams.z * wind.xyz;
			
			return pos;
		}

		v2f vert(appdata_full v)
		{
			v2f o;
			float4 tempWind;

			// 通过颜色通道判断顶点是否飘动，黑色不动
			float blendingFact = v.color.r;

			// 风的方向
			tempWind.xyz = mul((float3x3)unity_WorldToObject, _Wind.xyz);
			tempWind.w = _Wind.w * blendingFact;

			// 风的参数
			float4 windParams = float4(0, _WindEdgeFlutter, blendingFact.xx);

			// 风移动 按照_WindEdgeFlutterFreqScale 放大缩小偏移
			float windTime = _Time.y + float2(_WindEdgeFlutterFreqScale, 1);

			float4 mdlPos = AnimateVertex2(v.vertex, v.normal, windParams, tempWind, windTime);

			o.pos = UnityObjectToClipPos(mdlPos);
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

			o.L = normalize(ObjSpaceLightDir(mdlPos));
			o.N = normalize(mdlPos);

			o.color = v.color;
			return o;
		}

		ENDCG

		Pass
		{
			CGPROGRAM
			#pragma debug
			#pragma vertex vert
			#pragma fragment frag
			
			fixed4 frag(v2f i) : SV_Target
			{
				fixed3 N = normalize(i.N);
				fixed3 L = normalize(i.L);

				fixed3 diff = _LightColor0.rgb * saturate(dot(L,N)*0.5+0.5) + UNITY_LIGHTMODEL_AMBIENT.rgb;
			
				fixed4 c = tex2D(_MainTex, i.uv);
				c.rgb *= diff;
				return c;
			}
			ENDCG
		}
	}
}