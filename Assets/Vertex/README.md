## Vertex 变形
	可通过在Shader的顶点着色器中，对顶点位置进行修改。
	可以制作如跑酷地图，水面起伏，风吹旗帜等效果

##### 跑酷地图
- 跑酷地图中赛道的弯曲，可以通过shader来实现
- 先将顶点车到View空间，对其进行顶点偏移处理
	```
	float4 vPos = mul (UNITY_MATRIX_MV, v.vertex);
	float zOff = vPos.z/_Dist;
	vPos += _QOffset*zOff*zOff;
	o.vertex = mul (UNITY_MATRIX_P, vPos);
	```

##### 弧形地图
- 离中心点越远，Y值越小
- 通过先将坐标转到世界坐标，判断和中心点之前的距离，再算偏移
	```
	float3 worldPos = mul( unity_ObjectToWorld, v.vertex);
	float off = length(worldPos.xz) / _Dist;
	v.vertex.y += off * off * _Offset;
	```

##### 风吹旗
- 需要对模型的顶点添加顶点颜色来控制飘动的权重，黑色的颜色表示不动，白色颜色动。
- 当然也可以用一张黑白贴图来作为权重控制，然后在顶点中采样`tex2Dlod(_BlendTex, worldPos).r;` ，就像水面浮动一样

##### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Vertex/pic1.png)
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Vertex/pic2.png)