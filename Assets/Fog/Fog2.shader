﻿Shader "Study/Fog2"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _FogStart("Fog Start",float) = 10
        _FogEnd("Fog End",float) = 30
        _FogColor("Color",Color) = (0.6,0.6,0.6,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 worldPos: TEXCOORD1;
                float3 worldNormal:NORMAL;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _FogColor;

            float _FogStart;
            float _FogEnd;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos.w = o.worldPos.y;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 N = normalize(i.worldNormal);
                float3 L = normalize( UnityWorldSpaceLightDir(i.worldPos));
                float3 V = normalize( UnityWorldSpaceViewDir(i.worldPos));
                float3 H = normalize(L+V);

                float3 diff = _LightColor0.rgb * (dot(N,L) * 0.5 + 0.5 );
                float3 spec = _LightColor0.rgb * pow( saturate( dot(N,H) ), 0.5 * 128 );
                float frensel = 0.5 * pow( 1.0- saturate(dot(N,V)),3.0);

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * (UNITY_LIGHTMODEL_AMBIENT + float4(diff,1) + float4(spec,1));
                col.rgb = lerp(col.rgb,_LightColor0.rgb,frensel);
                // apply fog
                float factor = saturate((_FogEnd - i.worldPos.w) / (_FogEnd-_FogStart));
                col.rgb = lerp(col.rgb,_FogColor,factor);
                return col;
            }
            ENDCG
        }
    }
}
