## 雾
	雾的算法主要是通过和相机的距离，然后进行一次颜色叠加
	计算距离又有几种，分别是linear，exp2 等

#### 重点：
- 需要一个float值来作为计算距离
	- `UNITY_FOG_COORDS(1)` - 表示添加一个`float fogCoord:TEXCOORD1`变量，如果你已经用了`TEXCOORD1`了，相应就要改成其他2-3的`TEXCOORD`
	- `UNITY_TRANSFER_FOG(o,o.vertex);` - 在顶点计算中加入这行，用于雾的计算
	- `UNITY_APPLY_FOG(i.fogCoord, col);` - 在片元着色器中这句表示将上面的雾的数值和最后的`col`颜色值根据`i.fogCoord`这个距离值来进行混合运行
- 除了使用Unity自带的雾的计算方式 ，还可以修改雾的计算
	- 例如 `Fog2.shader`世界坐标的Y值越小雾越浓，可以制作城市在雾中的感觉
	- 还可以制作迷雾，中间区域显示，周围区域模糊

#### 实验结果：
![avatar](https://gitee.com/bingheliefeng/shaderstudy/raw/master/Assets/Fog/pic.png)